package com.threedsintegrator.sdk;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Debug;
import android.os.Environment;
import android.os.StatFs;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;

import com.scottyab.rootbeer.RootBeer;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECPoint;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Context.MODE_PRIVATE;
import static android.os.Build.*;
import static android.os.Build.DEVICE;
import static android.os.Build.MODEL;

public class ThreeDS implements ThreeDSService {

    private final String productionUrl = "https://api.3dsintegrator.com";
    private final String sandboxUrl = "https://api-sandbox.3dsintegrator.com/v2/";
    public static final String MY_TAG = "my_android_tag";
    public static final String SDK_VERSION ="1.0.0.0";
    public static final String SDK_REF_NUMBER="0313013";
//    private final String sandboxUrl = "https://api-sandbox.dev.3dsintegrator.com/v2/"; // dev sandbox

    private ArrayList<Warning> warnings;
    private String apiKey;
    private String locale;
    private String verbose;
    private String mode;
    private String baseUrl;
    private Context applicationContext;
    private ConfigParameters configParameters;
    private UiCustomization uiCustomization;
    private String sdkAppID;
    public Boolean isCleaned;
    private String authorization = "";
    private Boolean authentication = false;
    private ArrayList<String> errorMessages = new ArrayList<String>();

    public ThreeDS() {
        this.warnings = new ArrayList<Warning>();
    }

    public void initialize(Context applicationContext, ConfigParameters configParameters, String locale, UiCustomization uiCustomization) {
        this.applicationContext = applicationContext;
        this.configParameters = configParameters;
        this.locale = locale;
        this.uiCustomization = uiCustomization;
        this.isCleaned = false;
        this.apiKey = configParameters.getParamValue(ConfigParameters.DEFAULT_GROUP, "apiKey");
        this.verbose = configParameters.getParamValue(ConfigParameters.DEFAULT_GROUP, "verbose");
        this.mode = configParameters.getParamValue(ConfigParameters.DEFAULT_GROUP, "mode");
        if (this.mode == null || this.mode.equals("sandbox")) {
            this.baseUrl = sandboxUrl;
        } else {
            this.baseUrl = productionUrl;
        }

        boolean connection = isConnected();
        boolean emulator = isEmulator();
        boolean jailBroken = isJailBroken();
        boolean validOperatingSystemVersion = isValidOperatingSystemVersion();
        boolean debuggerConnected = isDebuggerConnected();
        boolean locationPermissions = hasPermission(Manifest.permission.ACCESS_FINE_LOCATION);
        boolean networkPermissions = hasPermission(Manifest.permission.ACCESS_NETWORK_STATE);
        //generate an AppID
        this.generateSDKAppID();

        if (this.verbose != null && this.verbose.equals("true")) {
            Log.i(MY_TAG, "initialize called");
            Log.i(MY_TAG, "API Key: " + apiKey);
            Log.i(MY_TAG, "Mode: " + configParameters.getParamValue("", "mode"));
            Log.i(MY_TAG, "Endpoint: " + baseUrl);
            Log.i(MY_TAG, "Emulator: " + String.valueOf(emulator));
            Log.i(MY_TAG, "Rooted: " + String.valueOf(jailBroken));
            Log.i(MY_TAG, "Valid OS Version: " + String.valueOf(validOperatingSystemVersion));
            Log.i(MY_TAG, "Has Internet Access: " + String.valueOf(networkPermissions));
            Log.i(MY_TAG, "Has Debugger: " + String.valueOf(debuggerConnected));
            Log.i(MY_TAG, "Has Internet: " + String.valueOf(connection));
            Log.i(MY_TAG, "Location Permissions: " + String.valueOf(locationPermissions));
            Log.i(MY_TAG, "# of Warnings: " + String.valueOf(this.warnings.size()));
            for (int i = 0; i < warnings.size(); i++) {
                Log.i(MY_TAG, "WARNING: " + String.valueOf(this.warnings.get(i).getId()));
            }
        }

    }

    @Override
    public Transaction createTransaction(String directoryServerID, String messageVersion) throws InvalidInputException, SDKNotInitializedException, SDKRuntimeException, InvalidAlgorithmParameterException, NoSuchAlgorithmException {
        if (!isCleaned) {
            Transaction transaction = new Transaction(directoryServerID, messageVersion,this.sdkAppID);
            String deviceInformation = getDeviceInformation();
            Log.i(MY_TAG, "Device information: " + deviceInformation);
            transaction.setDeviceData(deviceInformation);
            return transaction;
        } else {
            throw new SDKNotInitializedException("SDK not initialized");
        }
    }

    @Override
    public String getSDKVersion() throws SDKNotInitializedException, SDKRuntimeException {
        if (!isCleaned) {
            return SDK_VERSION;
        } else {
            throw new SDKNotInitializedException("SDK not initialized");
        }
    }

    public String getTestSDKVersion() throws SDKNotInitializedException, SDKRuntimeException {
        return SDK_VERSION;
    }

    @Override
    public void cleanup(Context applicationContext) throws SDKNotInitializedException {
        if (this.verbose != null && this.verbose.equals("true")) {
            Log.i(MY_TAG, "cleanup called");
        }
        //cleanup
        try {
            this.warnings = null;
            this.authentication = false;
            this.authorization = "";
            this.apiKey = null;
            this.locale = null;
            this.verbose = null;
            this.mode = null;
            this.baseUrl = null;
            this.applicationContext = null;
            this.configParameters = null;
            this.uiCustomization = null;
            this.sdkAppID = null;
            this.errorMessages = new ArrayList<String>();
        } finally {
            this.isCleaned = true;
        }
    }

    public boolean isJailBroken() {
        if (this.verbose != null && this.verbose.equals("true")) {
            Log.i(MY_TAG, "isJailBroken called");
        }
        RootBeer rootBeer = new RootBeer(this.applicationContext);
        if (rootBeer.isRootedWithoutBusyBoxCheck()) {
            Warning warning = new Warning("SW01", "The device is jailbroken.", Warning.Severity.HIGH);
            this.warnings.add(warning);
            return true;
        }
        return false;
    }

    public boolean isEmulator() {
        
        boolean result;
        result = FINGERPRINT.startsWith("generic")
                || FINGERPRINT.startsWith("unknown")
                || MODEL.contains("google_sdk")
                || MODEL.contains("Emulator")
                || MODEL.contains("Android SDK built for x86")
                || MANUFACTURER.contains("Genymotion")
                || (BRAND.startsWith("generic") && DEVICE.startsWith("generic"))
                || "google_sdk".equals(PRODUCT);

        if (result) {
            Warning warning = new Warning("SW03", "An emulator is being used to run the App.", Warning.Severity.HIGH);
            this.warnings.add(warning);
        }

        return result;
    }

    public boolean isValidOperatingSystemVersion() {
        if (VERSION.SDK_INT >= 23) {
            return true;
        }
        Warning warning = new Warning("SW05", "The OS or the OS version is not supported.", Warning.Severity.HIGH);
        this.warnings.add(warning);
        return false;
    }

    public boolean isDebuggerConnected() {
        boolean result;
        result = Debug.isDebuggerConnected();
        if (result) {
            Warning warning = new Warning("SW04", "A debugger is attached to the App.", Warning.Severity.MEDIUM);
            this.warnings.add(warning);
        }
        return result;
    }

    public boolean isConnected(){
        ConnectivityManager connectivityManager = (ConnectivityManager)this.applicationContext.getSystemService(Service.CONNECTIVITY_SERVICE);
        if (connectivityManager != null){
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            if (networkInfo != null){
                if (networkInfo.getState() == NetworkInfo.State.CONNECTED){
                    return true;
                }
            }
        }
        return false;
    }

    public boolean hasPermission(String permission) {
        if (ContextCompat.checkSelfPermission(this.applicationContext, permission) != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            Warning warning = new Warning("3DSW01", "App has no permissions", Warning.Severity.HIGH);
            this.warnings.add(warning);
            return false;
        }
        return true;

    }

    public List<Warning> getWarnings() {

        if (!isCleaned) {
            return this.warnings;
        } else {
            throw new SDKNotInitializedException("SDK not initialized");
        }
    }

    public List<Warning> getTestWarnings() {
        return this.warnings;
    }

    public String getBaseUrl() {
        return this.baseUrl;
    }

    public UiCustomization getUiCustomization() {
        return uiCustomization;
    }

    public ConfigParameters getConfigParameters() {
        return configParameters;
    }

    public String getLocale() {
        return locale;
    }

    private void generateSDKAppID() {
        SharedPreferences sharedPref = this.applicationContext.getSharedPreferences("SDK_APP_ID",Context.MODE_PRIVATE);
        String appID = sharedPref.getString("appID","NoAppID");
        if (appID.equals("NoAppID")){
            String newAppID = UUID.randomUUID().toString();
            this.sdkAppID = newAppID;
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("appID", newAppID);
            editor.apply();
        }
        else {
            this.sdkAppID = appID;
        }

        if (this.verbose != null && this.verbose.equals("true")) {
            Log.i(ThreeDS.MY_TAG,"SDK App ID: "+appID);
        }
    }

    public String getDeviceInformation(){
        try {
            DeviceInformation deviceInformation = new DeviceInformation(this.applicationContext,this.warnings);
            return deviceInformation.convertToJson();
        }
        catch (Exception ignore){}
        return "";

    }

    public void authorize() {
        Log.i(MY_TAG, "Authorize called");
        String bearer = "";
        Log.i(MY_TAG, "Endpoint: "+this.baseUrl);

        Retrofit retrofit;

        if (this.verbose != null && this.verbose.equals("true")) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.level(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(this.baseUrl)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        else {
            retrofit = new Retrofit.Builder()
                    .baseUrl(this.baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        ThreeDSIntegratorApi threeDSIntegratorApi = retrofit.create(ThreeDSIntegratorApi.class);
        Log.i(MY_TAG, "Api Key: "+this.apiKey);
        Call<AuthorizeResponse> call = threeDSIntegratorApi.authorize(this.apiKey);

        try {
            Response<AuthorizeResponse> response = call.execute();
            if (!response.isSuccessful()){
                Log.i(MY_TAG, "Authorize response not successful");
                Log.i(MY_TAG, "Error Code: "+response.code());
                try {
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    Log.i(MY_TAG,"Authorize Error Message: "+jObjError.getString("error"));
                    this.errorMessages.add(jObjError.getString("error"));

                }
                catch (JSONException e){
                    Log.i(MY_TAG, "JSON Exception: "+e.getMessage());
                }
                return;
            }else {
                Log.i(MY_TAG, "Authorize response successful");
                Log.i(MY_TAG, "Code: "+response.code());
                AuthorizeResponse authorizeResponse = response.body();
                Log.i(MY_TAG, "Authorize Message: "+authorizeResponse.getMessage());

                Headers responseHeaders = response.headers();
                bearer = responseHeaders.get("authorization");
                Log.i(MY_TAG, "Authorization: "+bearer);
            }

            this.authorization = bearer;
        }
        catch (IOException e){
            Log.i(MY_TAG, "Authorize Exception: "+e.getMessage());
        }

        Log.i(MY_TAG, "Authorize done");
    }

    public void authenticate(Transaction transaction) {
        //call authorize first
        this.authorize();

        Log.i(MY_TAG, "Authenticate called");

        TransactionData transactionData = transaction.getTransactionData();

        Retrofit retrofit;

        if (this.verbose != null && this.verbose.equals("true")) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.level(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(this.baseUrl)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        else {
            retrofit = new Retrofit.Builder()
                    .baseUrl(this.baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        ThreeDSIntegratorApi threeDSIntegratorApi = retrofit.create(ThreeDSIntegratorApi.class);
        ECPublicKey publickey =  (ECPublicKey) transaction.getTransactionKeyPair().getPublic();
        ECPoint ecp = publickey.getW();
        // to access X and Y you can use
        BigInteger x = ecp.getAffineX();
        BigInteger y = ecp.getAffineY();


        //setup public key class
        PublicKeyClass sdkEphemPubKey = new PublicKeyClass("EC","P-256",new String(x.toByteArray()),new String(y.toByteArray()));

        AuthenticationRequestParameters authenticationRequestParameters = transaction.getAuthenticationRequestParameters();

        SdkRequired sdkRequired = new SdkRequired(transaction.getSdkAppID(),sdkEphemPubKey,transaction.getSdkReferenceNumber(),"5",authenticationRequestParameters.getEncryptedDeviceData(),transaction.getSdkTransactionID());
        transactionData.setSdk(sdkRequired);


        if (this.verbose != null && this.verbose.equals("true")) {Log.i(MY_TAG, "Transaction Json Data: "+ transactionData.convertToJson());}
        Call<AuthenticateResponse> call = threeDSIntegratorApi.authenticate(this.apiKey,this.authorization, transactionData );

        try {
            Response<AuthenticateResponse> response = call.execute();
            if (!response.isSuccessful()){
                if (this.verbose != null && this.verbose.equals("true")) {
                    Log.i(MY_TAG, "Authenticate response not successful");
                    Log.i(MY_TAG, "Error Code: " + response.code());
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Log.i(MY_TAG,"Authenticate Error Message: "+jObjError.getString("error"));
                        this.errorMessages.add(jObjError.getString("error"));
                    }
                    catch (JSONException e){
                        Log.i(MY_TAG, "JSON Exception: "+e.getMessage());
                    }
//
                }
            }else {
                this.authentication = true;
                if (this.verbose != null && this.verbose.equals("true")) {
                    Log.i(MY_TAG, "Authenticate response successful");
                    Log.i(MY_TAG, "Code: " + response.code());
                }
                AuthenticateResponse authenticateResponse = response.body();
                Headers responseHeaders = response.headers();
                if (this.verbose != null && this.verbose.equals("true")) {
                    Log.i(MY_TAG, "Authenticate Body: " + authenticateResponse);
                    Log.i(MY_TAG, "Authenticate Headers: " + responseHeaders.toString());
                    Log.i(MY_TAG, "Authenticate Headers: \n"+responseHeaders.toString());
                    Log.i(MY_TAG, "ARes Authentication Value: "+authenticateResponse.getAuthenticationValue());
                    Log.i(MY_TAG, "ARes ECI: "+authenticateResponse.getEci());
                    Log.i(MY_TAG, "ARes Status: "+authenticateResponse.getStatus());
                    Log.i(MY_TAG, "ARes Protocol Version: "+authenticateResponse.getProtocolVersion());
                    Log.i(MY_TAG, "ARes dsTransId: "+authenticateResponse.getDsTransId());
                    Log.i(MY_TAG, "ARes acsTransId: "+authenticateResponse.getAcsTransId());
                    Log.i(MY_TAG, "ARes cardToken: "+authenticateResponse.getCardToken());
                }
            }
        }
        catch (IOException e){
            Log.i(MY_TAG, "Authenticate Exception: "+e.getMessage());
        }
    }

    public String getAuthorization() {
        return authorization;
    }

    public Boolean getAuthentication() {
        return authentication;
    }

    public ArrayList<String> getErrorMessages() {
        return errorMessages;
    }
}
