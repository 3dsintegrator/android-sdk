package com.threedsintegrator.sdk;

import android.content.Context;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class DeviceInformation {
    public String DV; //Data Version
    public DeviceData DD; //Device Data
    public DeviceParameterNotAvailable DPNA; //Device Parameter Not Available
    public ArrayList<String> SW = new ArrayList<String>(); //Security Warning

    public DeviceInformation(Context context, ArrayList<Warning> warnings) {
        this.DV = "1.1";
        this.DD = new DeviceData(context);
        this.DPNA = new DeviceParameterNotAvailable();
        for (Warning warning: warnings) {
            SW.add(warning.getId());
        }
    }

    public String convertToJson(){
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public String getSDKWarnings(){
        return "";
    }
}


