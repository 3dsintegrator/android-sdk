package com.threedsintegrator.sdk;

//Page 66
/**
 * The ToolbarCustomization class shall provide methods for the 3DS Requestor App
 * to pass toolbar customization parameters to the 3DS SDK. This class shall extend
 * the Customization class.
 */
public class ToolbarCustomization extends Customization{
    private String backgroundColor;
    private String headerText;
    private String buttonText;

    /**
     * Sets the background colour for the toolbar.
     * @param hexColorCode Colour code in Hex format
     * @throws InvalidInputException This exception shall be thrown if an input parameter is invalid.
     */
    public void setBackgroundColor(String hexColorCode) throws InvalidInputException{
        HexValidator hexValidator = new HexValidator();
        if (hexValidator.validate(hexColorCode)){
            this.backgroundColor = hexColorCode;
        }
        else {
            throw new InvalidInputException("Toolbar backgroundColor code is not Hex format");
        }
    }

    /**
     * Returns the background colour for the toolbar.
     * @return The getBackgroundColor method returns the background colour code (as a String) for the toolbar.
     */
    public String getBackgroundColor(){
        return this.backgroundColor;
    }

    /**
     * Sets the header text of the toolbar.
     * @param headerText Text for the header.
     * @throws InvalidInputException This exception shall be thrown if an input parameter is invalid.
     */
    public void setHeaderText(String headerText) throws InvalidInputException{
        try {
            this.headerText = headerText;
        }
        catch (Exception e){
            throw new InvalidInputException("Toolbar headerText input parameter is invalid.");
        }
    }

    /**
     * Returns the header text of the toolbar.
     * @return The getHeaderText method returns the header text (as a String) of the toolbar.
     */
    public String getHeaderText(){
        return this.headerText;
    }

    /**
     * Sets the button text of the toolbar.
     * @param buttonText Text for the button
     * @throws InvalidInputException This exception shall be thrown if an input parameter is invalid.
     */
    public void setButtonText(String buttonText) throws InvalidInputException{
        try {
            this.buttonText = buttonText;
        }
        catch (Exception e){
            throw new InvalidInputException("Toolbar buttonText input parameter is invalid.");
        }
    }

    /**
     * Returns the button text of the toolbar.
     * @return The getButtonText method returns the button text (as a String) of the toolbar.
     */
    public String getButtonText(){
        return this.buttonText;
    }
}
