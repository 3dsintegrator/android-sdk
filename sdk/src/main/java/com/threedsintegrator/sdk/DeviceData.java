package com.threedsintegrator.sdk;

import android.Manifest;
import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.format.Formatter;
import android.util.DisplayMetrics;
import android.util.Log;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class DeviceData {
    public String C001;//Platform
    public String C002;//Device Model
    public String C003;//OS Name
    public String C004;//Version
    public String C005;//Locale
    public String C006;//Time zone
    public String C007;//Advertising ID //TODO
    public String C008;//Screen Resolution
    public String C009;//Device Name
    public String C010;//IP Address
    public String C011;//Lat
    public String C012;//Long
    //TelephonyManager;
    public String A001 = "RE03";
    public String A002 = "RE03";
    public String A003 = "RE03";
    public String A004 = "RE03";
    public String A005 = "RE03";
    public String A006 = "RE03";
    public String A007 = "RE03";
    public String A008 = "RE03";
    public String A009 = "RE03";
    public String A010 = "RE03";
    public String A011 = "RE03";
    public String A012 = "RE03";
    public String A013 = "RE03";
    public String A014 = "RE03";
    public String A015 = "RE03";
    public String A016 = "RE03";
    public String A017 = "RE03";
    public String A018 = "RE03";
    public String A019 = "RE03";
    public String A020 = "RE03";
    public String A021 = "RE03";
    public String A022 = "RE03";
    public String A023 = "RE03";
    public String A024 = "RE03";
    public String A025 = "RE03";
    public String A026 = "RE03";
    public String A027 = "RE03";
    //Wifi Manager
    public String A028 = "RE03";
    public String A029 = "RE03";
    public String A030 = "RE03";
    public String A031 = "RE03";
    public String A032 = "RE03";
    public String A033 = "RE03";
    public String A034 = "RE03";
    public String A035 = "RE03";
    public String A036 = "RE03";
    public String A037 = "RE03";
    public String A038 = "RE03";
    //Bluetooth Manager
    public String A039 = "RE03";
    public String A040 = "RE03";
    public String A041 = "RE03";
    //Build
    public String A042;
    public String A043;
    public String A044;
    public String A045;
    public String A046;
    public String A047;
    public String A048;
    public String A049;
    public String A050;
    public String A051;
    public String A052;
    public String A053;
    public String A054;
    public String A055;
    public String A056;
    public String A057;
    public String A058;
    public String A059;
    //Build.VERSION
    public String A060;
    public String A061;
    public String A062;
    public String A063;
    public String A064;
    //Settings Secure
    public String A065;
    public String A066;
    public String A067;
    public String A068;
    public String A069;
    public String A070;
    public String A071;
    public String A072;
    public String A073;
    public String A074;
    public String A075;
    public String A076;
    public String A077;
    public String A078;
    public String A079;
    public String A080;
    public String A081;
    public String A082;
    public String A083;
    //Settings Global
    public String A084;
    public String A085;
    public String A086;
    public String A087;
    public String A088;
    public String A089;
    public String A090;
    public String A091;
    public String A092;
    public String A093;
    public String A094;
    public String A095;
    public String A096;
    public String A097;
    public String A098;
    //System setting
    public String A099;
    public String A100;
    public String A101;
    public String A102;
    public String A103;
    public String A104;
    public String A105;
    public String A106;
    public String A107;
    public String A108;
    public String A109;
    public String A110;
    public String A111;
    public String A112;
    public String A113;
    public String A114;
    public String A115;
    public String A116;
    public String A117;
    public String A118;
    public String A119;
    public String A120;
    public String A121;
    public String A122;
    public String A123;
    //Package manager
    public String A124;
    public String A125;
    public String A126;
    public String A127;
    public String A128;
    //Environment
    public String A129;
    //Locale
    public String A130;
    //DisplayMetrics
    public String A131;
    public String A132;
    public String A133;
    public String A134;
    public String A135;
    //StatFs
    public String A136;

    @SuppressLint("MissingPermission")
    public DeviceData(Context context) {
        this.C001 = "Android";
        this.C002 = Build.MODEL;
        this.C003 = System.getProperty("os.name");
        this.C004 = Build.VERSION.RELEASE;
        this.C005 = Locale.getDefault().getLanguage();
        this.C006 = TimeZone.getDefault().getDisplayName();
        this.C007 = "";
        this.C008 = this.getScreenResolution();
        this.C009 = this.getDeviceName();
        this.C010 = this.getIPAddress();
        this.C011 = this.getLatitude(context);
        this.C012 = this.getLongitude(context);

        try {
            if (context.checkCallingOrSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                TelephonyManager telephonyManager = (TelephonyManager)context.getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
                this.A001 = (telephonyManager.getDeviceId() == null ? "RE03" : telephonyManager.getDeviceId());
                this.A002 = (telephonyManager.getSubscriberId() == null ? "RE03" : telephonyManager.getSubscriberId());
                this.A003 = (telephonyManager.getDeviceSoftwareVersion() == null ? "RE03" : telephonyManager.getDeviceSoftwareVersion());
                this.A004 =  (telephonyManager.getGroupIdLevel1() == null ? "RE03" : telephonyManager.getGroupIdLevel1());
                this.A005 =  (telephonyManager.getLine1Number() == null ? "RE03" : telephonyManager.getLine1Number());
                this.A006 =  (telephonyManager.getMmsUAProfUrl() == null ? "RE03" : telephonyManager.getMmsUAProfUrl());
                this.A007 =  (telephonyManager.getMmsUserAgent() == null ? "RE03" : telephonyManager.getMmsUserAgent());
                this.A008 =  (telephonyManager.getNetworkCountryIso() == null ? "RE03" : telephonyManager.getNetworkCountryIso());
                this.A009 =  (telephonyManager.getNetworkOperator() == null ? "RE03" : telephonyManager.getNetworkOperator());
                this.A010 =  (telephonyManager.getNetworkOperatorName() == null ? "RE03" : telephonyManager.getNetworkOperatorName());
                this.A011 =  Integer.toString(telephonyManager.getNetworkType());
                this.A012 =  Integer.toString(telephonyManager.getPhoneCount());
                this.A013 =  Integer.toString(telephonyManager.getPhoneType());
                this.A014 =  (telephonyManager.getSimCountryIso() == null ? "RE03" : telephonyManager.getSimCountryIso());
                this.A015 =  (telephonyManager.getSimOperator() == null ? "RE03" : telephonyManager.getSimOperator());
                this.A016 =  (telephonyManager.getSimOperatorName() == null ? "RE03" : telephonyManager.getSimOperatorName());
                this.A017 =  (telephonyManager.getSimSerialNumber() == null ? "RE03" : telephonyManager.getSimSerialNumber());
                this.A018 =  Integer.toString(telephonyManager.getSimState());
                this.A019 =  (telephonyManager.getVoiceMailAlphaTag() == null ? "RE03" : telephonyManager.getVoiceMailAlphaTag());
                this.A020 =  (telephonyManager.getVoiceMailNumber() == null ? "RE03" : telephonyManager.getVoiceMailNumber());
                this.A021 =  Boolean.toString(telephonyManager.hasIccCard());
                this.A022 =  Boolean.toString(telephonyManager.isHearingAidCompatibilitySupported());
                this.A023 =  Boolean.toString(telephonyManager.isNetworkRoaming());
                this.A024 =  Boolean.toString(telephonyManager.isSmsCapable());
                this.A025 =  Boolean.toString(telephonyManager.isTtyModeSupported());
                this.A026 =  Boolean.toString(telephonyManager.isVoiceCapable());
                this.A027 =  Boolean.toString(telephonyManager.isWorldPhone());
            }

            if (context.checkCallingOrSelfPermission(Manifest.permission.ACCESS_WIFI_STATE) == PackageManager.PERMISSION_GRANTED) {
                WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

                if (wifiManager != null){
                    WifiInfo wifiInfo = wifiManager.getConnectionInfo();

                    this.A028 = (wifiInfo.getMacAddress() == null ? "RE03" : wifiInfo.getMacAddress());
                    this.A029 = (wifiInfo.getBSSID() == null ? "RE03" : wifiInfo.getBSSID());
                    this.A030 = (wifiInfo.getSSID() == null ? "RE03" : wifiInfo.getSSID());
                    this.A031 = Integer.toString(wifiInfo.getNetworkId());
                    this.A032 = Boolean.toString(wifiManager.is5GHzBandSupported());
                    this.A033 = Boolean.toString(wifiManager.isDeviceToApRttSupported());
                    this.A034 = Boolean.toString(wifiManager.isEnhancedPowerReportingSupported());
                    this.A035 = Boolean.toString(wifiManager.isP2pSupported());
                    this.A036 = Boolean.toString(wifiManager.isPreferredNetworkOffloadSupported());
                    this.A037 = Boolean.toString(wifiManager.isScanAlwaysAvailable());
                    this.A038 = Boolean.toString(wifiManager.isTdlsSupported());
                }
            }

            if (context.checkCallingOrSelfPermission(Manifest.permission.BLUETOOTH) == PackageManager.PERMISSION_GRANTED) {
                BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

                if (bluetoothAdapter != null){
                    this.A039 = (bluetoothAdapter.getAddress() == null ? "RE03" : bluetoothAdapter.getAddress());
                    this.A040 = (bluetoothAdapter.getBondedDevices() == null ? "RE03" : bluetoothAdapter.getBondedDevices().toString());
                    this.A041 = Boolean.toString(bluetoothAdapter.isEnabled());
                }
            }

            //Build
            this.A042 = Build.BOARD;
            this.A043 = Build.BOOTLOADER;
            this.A044 = Build.BRAND;
            this.A045 = Build.DEVICE;
            this.A046 = Build.DISPLAY;
            this.A047 = Build.FINGERPRINT;
            this.A048 = Build.HARDWARE;
            this.A049 = Build.ID;
            this.A050 = Build.MANUFACTURER;
            this.A051 = Build.PRODUCT;
            this.A052 = Build.getRadioVersion();
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                this.A053 = Build.getSerial();
            } else{
                this.A053 = "RE02";
            }
            this.A054 = Arrays.toString(Build.SUPPORTED_32_BIT_ABIS);
            this.A055 = Arrays.toString(Build.SUPPORTED_64_BIT_ABIS);
            this.A056 = Build.TAGS;
            this.A057 = Long.toString(Build.TIME);
            this.A058 = Build.TYPE;
            this.A059 = Build.USER;

            //Build.VERSION
            this.A060 = Build.VERSION.CODENAME;
            this.A061 = Build.VERSION.INCREMENTAL;
            this.A062 = Integer.toString(Build.VERSION.PREVIEW_SDK_INT);
            this.A063 = Integer.toString(Build.VERSION.SDK_INT);
            this.A064 = Build.VERSION.SECURITY_PATCH;

            //Settings Secure
            this.A065 = Settings.Secure.ACCESSIBILITY_DISPLAY_INVERSION_ENABLED;
            this.A066 = Settings.Secure.ACCESSIBILITY_ENABLED;
            this.A067 = Settings.Secure.ACCESSIBILITY_SPEAK_PASSWORD;
            this.A068 = Settings.Secure.ALLOWED_GEOLOCATION_ORIGINS;
            this.A069 = Settings.Secure.ANDROID_ID;
            this.A070 = Settings.Global.DATA_ROAMING;
            this.A071 = Settings.Secure.DEFAULT_INPUT_METHOD;
            this.A072 = Settings.Global.DEVICE_PROVISIONED;
            this.A073 = Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES;
            this.A074 = Settings.Secure.ENABLED_INPUT_METHODS;
            this.A075 = Settings.Secure.INPUT_METHOD_SELECTOR_VISIBILITY;
            this.A076 = "RE02";
            this.A077 = Settings.Secure.LOCATION_MODE;
            this.A078 = Settings.Secure.SKIP_FIRST_USE_HINTS;
            this.A079 = "RE02";
            this.A080 = Settings.Secure.TTS_DEFAULT_PITCH;
            this.A081 = Settings.Secure.TTS_DEFAULT_RATE;
            this.A082 = Settings.Secure.TTS_DEFAULT_SYNTH;
            this.A083 = Settings.Secure.TTS_ENABLED_PLUGINS;

            //Settings Globals
            this.A084 = Settings.Global.ADB_ENABLED;
            this.A085 = Settings.Global.AIRPLANE_MODE_RADIOS;
            this.A086 = Settings.Global.ALWAYS_FINISH_ACTIVITIES;
            this.A087 = Settings.Global.ANIMATOR_DURATION_SCALE;
            this.A088 = Settings.Global.AUTO_TIME;
            this.A089 = Settings.Global.AUTO_TIME_ZONE;
            this.A090 = Settings.Global.DEVELOPMENT_SETTINGS_ENABLED;
            this.A091 = Settings.Global.HTTP_PROXY;
            this.A092 = Settings.Global.NETWORK_PREFERENCE;
            this.A093 = Settings.Global.STAY_ON_WHILE_PLUGGED_IN;
            this.A094 = Settings.Global.TRANSITION_ANIMATION_SCALE;
            this.A095 = Settings.Global.USB_MASS_STORAGE_ENABLED;
            this.A096 = Settings.Global.USE_GOOGLE_MAIL;
            this.A097 = Settings.Global.WAIT_FOR_DEBUGGER;
            this.A098 = Settings.Global.WIFI_NETWORKS_AVAILABLE_NOTIFICATION_ON;

            //Settings System
            this.A099 = Settings.System.ACCELEROMETER_ROTATION;
            this.A100 = Settings.System.BLUETOOTH_DISCOVERABILITY;
            this.A101 = Settings.System.BLUETOOTH_DISCOVERABILITY_TIMEOUT;
            this.A102 = Settings.System.DATE_FORMAT;
            this.A103 = Settings.System.DTMF_TONE_TYPE_WHEN_DIALING;
            this.A104 = Settings.System.DTMF_TONE_WHEN_DIALING;
            this.A105 = Settings.System.END_BUTTON_BEHAVIOR;
            this.A106 = Settings.System.FONT_SCALE;
            this.A107 = Settings.System.HAPTIC_FEEDBACK_ENABLED;
            this.A108 = Settings.System.MODE_RINGER_STREAMS_AFFECTED;
            this.A109 = Settings.System.NOTIFICATION_SOUND;
            this.A110 = Settings.System.MUTE_STREAMS_AFFECTED;
            this.A111 = Settings.System.RINGTONE;
            this.A112 = Settings.System.SCREEN_BRIGHTNESS;
            this.A113 = Settings.System.SCREEN_BRIGHTNESS_MODE;
            this.A114 = Settings.System.SCREEN_OFF_TIMEOUT;
            this.A115 = Settings.System.SOUND_EFFECTS_ENABLED;
            this.A116 = Settings.System.TEXT_AUTO_CAPS;
            this.A117 = Settings.System.TEXT_AUTO_PUNCTUATE;
            this.A118 = Settings.System.TEXT_AUTO_REPLACE;
            this.A119 = Settings.System.TEXT_SHOW_PASSWORD;
            this.A120 = Settings.System.TIME_12_24;
            this.A121 = Settings.System.USER_ROTATION;
            this.A122 = Settings.System.VIBRATE_ON;
            this.A123 = Settings.System.VIBRATE_WHEN_RINGING;

            //Package Manager
            PackageManager packageManager = context.getPackageManager();
            this.A124 = Boolean.toString(packageManager.isSafeMode());
            this.A125 = packageManager.getInstalledApplications(0).toString();
            this.A126 = packageManager.getInstallerPackageName(context.getPackageName());
            this.A127 = Integer.toString(packageManager.getSystemAvailableFeatures().length);
            this.A128 = Integer.toString(packageManager.getSystemSharedLibraryNames().length);

            //Environment
            this.A129 = Environment.getExternalStorageState();

            //Locale
            this.A130 = Integer.toString(Locale.getAvailableLocales().length);

            //Display Metrics
            DisplayMetrics displayMetrics = new DisplayMetrics();
            this.A131 = Float.toString(displayMetrics.density);
            this.A132 = Float.toString(displayMetrics.densityDpi);
            this.A133 = Float.toString(displayMetrics.scaledDensity);
            this.A134 = Float.toString(displayMetrics.xdpi);
            this.A135 = Float.toString(displayMetrics.ydpi);

            //StatFs
            StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
            this.A136 = Float.toString(statFs.getTotalBytes());
        }
        catch (Exception ignore){}

    }

    private String getLatitude(Context context){
        LocationManager locationManager = (LocationManager)context.getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        try {
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null){
                double latitude = location.getLatitude();
                Log.i(ThreeDS.MY_TAG,"LAT: "+Double.toString(latitude));
                return  Double.toString(latitude);
            }
//            else {
//                Log.i(ThreeDS.MY_TAG,"Location is null");
//            }
        }
        catch (SecurityException ignore){}
        return "RE03";
    }

    private String getLongitude(Context context){
        LocationManager locationManager = (LocationManager)context.getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        try {
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null){
                double longitude = location.getLongitude();
                Log.i(ThreeDS.MY_TAG,"LON: "+Double.toString(longitude));
                return  Double.toString(longitude);
            }
//            else {
//                Log.i(ThreeDS.MY_TAG,"Location is null");
//            }
        }
        catch (SecurityException ignore){}
        return "RE03";
    }

    private String getScreenResolution(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        return Integer.toString(displayMetrics.widthPixels) + " x " + Integer.toString(displayMetrics.heightPixels);
    }

    private String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    private String getIPAddress() {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        boolean isIPv4 = sAddr.indexOf(':') < 0;

                        return sAddr;
                    }
                }
            }
        } catch (Exception ignored) {

        }
        return "RE02";

    }
}

