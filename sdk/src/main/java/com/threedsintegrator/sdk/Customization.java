package com.threedsintegrator.sdk;

//Page 59
/**
 * The Customization class shall serve as a superclass for the ButtonCustomization class,
 * ToolbarCustomization class, LabelCustomization class, and TextBoxCustomization class.
 * This class shall provide methods to pass UI customization parameters to the 3DS SDK.
 */
public class Customization {
    private String fontName;
    private String hexColorCode;
    private int fontSize;

    /**
     * Sets the font type for a UI element.
     * @param fontName Font type for the UI element.
     * @throws InvalidInputException This exception shall be thrown if an input parameter is invalid.
     */
    public void setTextFontName(String fontName) throws InvalidInputException{
        try {
            this.fontName = fontName;
        }
        catch (Exception e){
            throw new InvalidInputException("FontName input parameter is invalid.");
        }
    }

    /**
     * Sets the text color for a UI element.
     * @param hexColorCode Color code in Hex format.
     * @throws InvalidInputException This exception shall be thrown if an input parameter is invalid.
     */
    public void setTextColor(String hexColorCode) throws InvalidInputException{
        try {
            this.hexColorCode = hexColorCode;
        }
        catch (Exception e){
            throw new InvalidInputException("HexColorCode input parameter is invalid.");
        }
    }

    /**
     * Sets the font size for a UI element.
     * @param fontSize Font size for the UI element.
     * @throws InvalidInputException This exception shall be thrown if an input parameter is invalid.
     */
    public void setTextFontSize(int fontSize) throws InvalidInputException {
        try {
            this.fontSize = fontSize;
        }
        catch (Exception e){
            throw new InvalidInputException("FontSize input parameter is invalid.");
        }
    }

    /**
     * Returns the font type for a UI element.
     * @return The getTextFontName method returns the font type as a string.
     */
    public String getTextFontName(){return this.fontName;}

    /**
     * Returns the text color for a UI element.
     * @return The getTextColor method returns the hex color code as a string.
     */
    public String getTextColor(){return this.hexColorCode;}

    /**
     * Returns the font size for a UI element
     * @return The getTextFontSize method returns the font size as an integer.
     */
    public int getTextFontSize(){return this.fontSize;}
}
