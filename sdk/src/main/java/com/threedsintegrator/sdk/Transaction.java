package com.threedsintegrator.sdk;

import android.app.Activity;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECGenParameterSpec;
import java.util.UUID;
import java.util.List;

import javax.crypto.SecretKey;

/*
The actual AReq construction is outside the SDK scope and depends on the 3DS Server
implementation. The interface between the 3DS Requestor App and 3DS Server
(or other access point in 3DS Environment) is not defined in the Core or SDK
Specifications.
 */
public class Transaction implements TransactionInterface {
    private String sdkEphemeralPublicKey;
    private String sdkEphemeralPrivateKey;
    private String directoryServerID;
    private String messageVersion;
    private String sdkTransactionID;
    private String sdkReferenceNumber;
    private String sdkAppID;
    private String deviceData;
    private TransactionData transactionData;

    private PublicKey ACSPublicKey =null;


    public Boolean isClosed = false;
    public KeyPair transactionKeyPair;
    public KeyPair transactionSecretKey;


    private Activity challengeActivity;
    
    public Transaction(String directoryServerID, String messageVersion, String sdkAppID) throws InvalidAlgorithmParameterException, NoSuchAlgorithmException {
        this.directoryServerID = directoryServerID;
        this.messageVersion = messageVersion;
        //set sdk transaction id
        this.sdkTransactionID = UUID.randomUUID().toString();
        this.sdkAppID = sdkAppID;
        this.sdkReferenceNumber = "3DS";

        //Generates a fresh ephemeral key pair
        KeyPairGenerator keyPairGenerator = null;
        try {
            keyPairGenerator = KeyPairGenerator.getInstance("EC");
            keyPairGenerator.initialize(256);
            KeyPair keyPair = keyPairGenerator.generateKeyPair();
            this.transactionKeyPair = keyPair;
            ECPublicKey publicKey = (ECPublicKey) keyPair.getPublic();
            ECPrivateKey privateKey = (ECPrivateKey) keyPair.getPrivate();

            this.sdkEphemeralPublicKey = publicKey.toString();
            this.sdkEphemeralPrivateKey = privateKey.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }

    public AuthenticationRequestParameters getAuthenticationRequestParameters() {
        if (!this.isClosed) {
            return new AuthenticationRequestParameters(this.sdkTransactionID, this.deviceData, this.sdkEphemeralPublicKey, this.sdkAppID, this.sdkReferenceNumber,  this.messageVersion, this.transactionKeyPair);
        } else {
            throw new SDKRuntimeException("Transaction Closed", "001");
        }
    }

    public void doChallenge(Activity currentActivity, ChallengeParameters challengeParameters, final ChallengeStatusReceiver challengeStatusReceiver, int timeOut) throws InvalidInputException, SDKRuntimeException {
        //pg 49 - The minimum timeout interval shall be 5 minutes
        if (timeOut < 5) {
            throw new InvalidInputException("Timeout should not be less than 5 minutes");
        }

        //start timer
        int timeOutInMilliseconds = timeOut * 60000;
        CountDownTimer countDownTimer = new CountDownTimer(timeOutInMilliseconds,1000) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                challengeStatusReceiver.timedout();
            }
        }.start();

        //check if DS-CA is present
        String directoryServerID = this.getDirectoryServerID();
        JSONObject CReq = null;
        JSONObject CRes = null;


        //validate ACS signed content
        TransactionUtils.jwsValidateSignatureAndReturnBody(challengeParameters.acsSignedContent);

        //perform DH exchange
        TransactionUtils transactionUtils = new TransactionUtils();

        //set ACS public key
        transactionUtils.setReceiverPublicKey(this.ACSPublicKey);

        //encrypt and send creq (while loop until valid or time expires)
        //encrypt the message
        try{
            CReq = new JSONObject();
            CReq.put("threeDSServerTransID",challengeParameters.serverTransactionID);
            CReq.put("acsTransID",challengeParameters.acsTransactionID);
            CReq.put("messageType","CReq");
            CReq.put("messageVersion","2.1.0");
            //have to see how these are set- do I generate my own??
            CReq.put("sdkTransID","001");//generate uid
            CReq.put("sdkCounterStoA","001");
        }
        catch (Exception e){
            Log.i(ThreeDS.MY_TAG,e.getMessage());
        }

        //send creq, receive and decrypt cres
        String encryptedCReq = transactionUtils.encrypt(CReq.toString());
        String EncryptedACSResponse = TransactionUtils.sendCReq(challengeParameters.threeDSRequestorAppURL,encryptedCReq);
        String DecryptedACSResponse = transactionUtils.decrypt(EncryptedACSResponse);

        //convert to json object
        try {
            CRes = new JSONObject(DecryptedACSResponse);
        }catch (JSONException err){
            Log.d("Error", err.toString());
        }

        //get info and display challenge screen to cardholder from CRes
        //EMVCo_3DS_Json_210_0418.pdf pg 13
        

        //clean up resources

    }

    public ProgressBar getProgressView(Activity currentActivity) throws InvalidInputException, SDKRuntimeException {
        if (!this.isClosed) {
            ProgressBar progressBar = new ProgressBar(currentActivity, null, android.R.attr.progressBarStyleLarge);
            progressBar.setIndeterminate(true);
            progressBar.setVisibility(View.VISIBLE);

            return progressBar;
        } else {
            throw new SDKRuntimeException("Transaction Closed", "001");
        }
    }

    public void close() {
        try {
            this.directoryServerID = null;
            this.messageVersion = null;
            this.sdkTransactionID = null;
            this.sdkEphemeralPublicKey = null;
            this.sdkAppID = null;
            this.sdkReferenceNumber = null;

            transactionKeyPair = null;
            transactionSecretKey = null;
        } finally {
            this.isClosed = true;

        }
    }

    public String getDirectoryServerID() {
        return directoryServerID;
    }

    public TransactionData getTransactionData() {
        return transactionData;
    }

    public void setTransactionData(TransactionData transactionData) {
        this.transactionData = transactionData;
    }

    public String getSdkEphemeralPublicKey() {
        return sdkEphemeralPublicKey;
    }

    public String getSdkEphemeralPrivateKey() {
        return sdkEphemeralPrivateKey;
    }

    public KeyPair getTransactionKeyPair() {
        return transactionKeyPair;
    }

    public String getSdkAppID() {
        return sdkAppID;
    }

    public String getMessageVersion() {
        return messageVersion;
    }

    public String getSdkTransactionID() {
        return sdkTransactionID;
    }

    public String getSdkReferenceNumber() {
        return sdkReferenceNumber;
    }

    public void setDeviceData(String deviceData) {
        this.deviceData = deviceData;
    }
}