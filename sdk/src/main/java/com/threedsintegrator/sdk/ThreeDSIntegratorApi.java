package com.threedsintegrator.sdk;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ThreeDSIntegratorApi {
//    Call<List<>>

    @Headers("Content-Type: application/json")
    @POST("authorize")
    Call<AuthorizeResponse> authorize(@Header("X-3DS-API-KEY") String apiKey);

    @Headers({"Content-Type: application/json","Referer: https://example.com","Accept: application/json"})
    @POST("authenticate/sdk")
    Call<AuthenticateResponse> authenticate(
            @Header("X-3DS-API-KEY") String apiKey,
            @Header("Authorization") String authorization,
            @Body TransactionData transactionData);
}
