package com.threedsintegrator.sdk;

//Page 101
public class SDKAlreadyInitializeException extends RuntimeException {
    public SDKAlreadyInitializeException(String message) {super(message); }
}
