package com.threedsintegrator.sdk;

import java.util.HashMap;

//Page 38
public class ConfigParameters {

    public static final String DEFAULT_GROUP = "default";
    private HashMap<String, String> params = new HashMap<String, String>();

    /**
     * @param group Group to which the configuration parameter is to be added.
     * @param paramName Name of the configuration parameter.
     * @param paramValue Value of the configuration parameter
     * @throws InvalidInputException This exception shall be thrown if paramName is null or if the parameter in the group is duplicate.
     */
    public void addParam(String group, String paramName, String paramValue) throws InvalidInputException {
        if (paramName == null){
            throw new InvalidInputException("The paramName cannot be null.");
        }

        if (paramValue == null){
            throw new InvalidInputException("The paramValue cannot be null.");
        }

        if (this.getParamValue(group,paramName) != null && !this.getParamValue(group,paramName).equals("")){
            throw new InvalidInputException("The parameter in the group is a duplicate.");
        }

        if (group != null && !group.isEmpty()){
            params.put(group+paramName,paramValue);
        }
        else {
            params.put(DEFAULT_GROUP+paramName,paramValue);
        }
    }

    /**
     *
     * @param group Group from which the configuration parameter’s value is to be returned.
     * @param paramName Name of the configuration parameter.
     * @return The value of the specified configuration parameter as a string.
     * @throws InvalidInputException This exception shall be thrown if paramName is null.
     */
    public String getParamValue(String group, String paramName) throws InvalidInputException {
        String value;
        if (paramName == null){
            throw new InvalidInputException("The paramName cannot be null.");
        }

        if (group != null && !group.isEmpty()){
            value = params.get(group+paramName);
            return value;
        }
        else if (group == null) {
            value = params.get(DEFAULT_GROUP+paramName);
            return value;
        }
        return null;
    }

    /**
     *
     * @param group Group from which the configuration parameter is to be removed.
     * @param paramName Name of the configuration parameter.
     * @return Should return the value of the parameter that it removes. If the parameter is not found in the specified group, then this method returns null.
     * @throws InvalidInputException This exception shall be thrown if paramName is null.
     */
    public String removeParam(String group, String paramName) throws InvalidInputException {
        String value;
        if (paramName == null){
            throw new InvalidInputException("The paramName cannot be null.");
        }

        if (group != null && !group.isEmpty()){
            value = params.get(group+paramName);
            params.remove(group+paramName);
            return value;
        }
        else if (group == null){
            value = params.get(DEFAULT_GROUP+paramName);
            params.remove(DEFAULT_GROUP+paramName);
            return value;
        }
        return null;
    }

}
