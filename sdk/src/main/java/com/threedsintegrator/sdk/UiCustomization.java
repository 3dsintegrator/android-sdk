package com.threedsintegrator.sdk;

//Page 51
/**
 * The UiCustomization class shall provide the functionality required to customize the 3DS SDK UI elements.
 * An object of this class holds various UI-related parameters.
 */
public class UiCustomization {
    private ButtonCustomization buttonCustomization;
    private ToolbarCustomization toolbarCustomization;
    private LabelCustomization labelCustomization;
    private TextBoxCustomization textBoxCustomization;

    public enum ButtonType {
        SUBMIT,
        CONTINUE,
        NEXT,
        CANCEL,
        RESEND;
    }

    /**
     * Sets the attributes of a ButtonCustomization object for a particular button type.
     * Sets the attributes of a ButtonCustomization object for an implementer-specific button type.
     * @param buttonCustomization A ButtonCustomization object.
     * @param buttonType Implementer-specific button type. The value of the buttonType is case-insensitive.
     * @throws InvalidInputException This exception shall be thrown if an input parameter is invalid.
     */
    public void setButtonCustomization(ButtonCustomization buttonCustomization, ButtonType buttonType) throws  InvalidInputException{
        try {
            this.buttonCustomization = buttonCustomization;
        }
        catch (Exception e){
            throw new InvalidInputException("ButtonCustomization input parameter is invalid.");
        }
    }

    /**
     * Sets the attributes of a ToolbarCustomization object.
     * @param toolbarCustomization A ToolbarCustomization object.
     * @throws InvalidInputException This exception shall be thrown if an input parameter is invalid.
     */
    public void setToolbarCustomization(ToolbarCustomization toolbarCustomization) throws InvalidInputException{
        try {
            this.toolbarCustomization = toolbarCustomization;
        }
        catch (Exception e){
            throw new InvalidInputException("ToolbarCustomization input parameter is invalid.");
        }
    }

    /**
     * Sets the attributes of a LabelCustomization object.
     * @param labelCustomization A LabelCustomization object.
     * @throws InvalidInputException This exception shall be thrown if an input parameter is invalid.
     */
    public void setLabelCustomization(LabelCustomization labelCustomization) throws InvalidInputException{
        try {
            this.labelCustomization = labelCustomization;
        }
        catch (Exception e){
            throw new InvalidInputException("LabelCustomization input parameter is invalid.");
        }
    }

    /**
     * Sets the attributes of a TextBoxCustomization object.
     * @param textBoxCustomization ATextBoxCustomizationobject.
     * @throws InvalidInputException This exception shall be thrown if an input parameter is invalid.
     */
    public void setTextBoxCustomization(TextBoxCustomization textBoxCustomization) throws InvalidInputException{
        try {
            this.textBoxCustomization = textBoxCustomization;
        }
        catch (Exception e){
            throw new InvalidInputException("TextBoxCustomization input parameter is invalid.");
        }
    }

    /**
     * Returns a ButtonCustomization object.
     * Returns a ButtonCustomization object for an implementer-specific button type.
     * @param buttonType
     * @return
     */
    public ButtonCustomization getButtonCustomization(ButtonType buttonType){
        return this.buttonCustomization;
    }

    /**
     * Returns a ToolbarCustomization object.
     * @return
     */
    public ToolbarCustomization getToolbarCustomization(){
        return this.toolbarCustomization;
    }

    /**
     * Returns a LabelCustomization object.
     * @return
     */
    public LabelCustomization getLabelCustomization(){
        return this.labelCustomization;
    }

    /**
     * Returns a TextBoxCustomization object.
     * @return
     */
    public TextBoxCustomization getTextBoxCustomization(){
        return this.textBoxCustomization;
    }
}
