package com.threedsintegrator.sdk;

//Page 77
public class ChallengeParameters {
    public String serverTransactionID;
    public String acsTransactionID;
    public String acsRefNumber;
    public String acsSignedContent;
    public String threeDSRequestorAppURL;

    public void set3DSServerTransactionID(String serverTransactionID){
        this.serverTransactionID = serverTransactionID;
    }
    public void setAcsTransactionID(String acsTransactionID){
        this.acsTransactionID = acsTransactionID;
    }
    public void setAcsRefNumber(String acsRefNumber){
        this.acsRefNumber = acsRefNumber;
    }
    public void setAcsSignedContent(String acsSignedContent){
        this.acsSignedContent= acsSignedContent;
    }
    public void setThreeDSRequestorAppURL(String threeDSRequestorAppURL){
        this.threeDSRequestorAppURL = threeDSRequestorAppURL;
    }

    public String get3DSServerTransactionID(){
        return this.serverTransactionID;
    }
    public String getAcsTransactionID(){
        return this.acsTransactionID;
    }
    public String getAcsRefNumber(){
        return this.acsRefNumber;
    }
    public String getAcsSignedContent(){
        return this.acsSignedContent;
    }
    public String getThreeDSRequestorAppURL(){
        return this.threeDSRequestorAppURL;
    }
}
