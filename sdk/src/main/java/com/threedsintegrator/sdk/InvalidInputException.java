package com.threedsintegrator.sdk;

//Pg 36
public class InvalidInputException extends RuntimeException {
    public InvalidInputException(String message) {
        super(message);
    }
}
