package com.threedsintegrator.sdk;

/**
 * The RuntimeErrorEvent class shall hold details of run-time errors
 * that are encountered by the 3DS SDK during authentication.
 * Examples of RuntimeErrors: ACS is unreachable, Unparseable message, Network issues
 */
public class RuntimeErrorEvent {

    private String errorCode;
    private String errorMessage;

    public RuntimeErrorEvent(String errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }
}
