package com.threedsintegrator.sdk;

//Page 70
/**
 * The LabelCustomization class shall provide methods for the 3DS Requestor App
 * to pass label customization parameters to the 3DS SDK. This class shall extend
 * the Customization class.
 */
public class LabelCustomization extends Customization {

    /**
     * Sets the colour of the heading label text.
     * @param hexColorCode Colour code in Hex format.
     * @throws InvalidInputException This exception shall be thrown if an input parameter is invalid.
     */
    public void setHeadingTextColor(String hexColorCode) throws InvalidInputException{
        this.setTextColor(hexColorCode);
    }

    /**
     * Sets the font type of the heading label text.
     * @param fontName Font type for the heading label text.
     * @throws InvalidInputException This exception shall be thrown if an input parameter is invalid.
     */
    public void setHeadingTextFontName(String fontName) throws  InvalidInputException{
        this.setTextFontName(fontName);
    }

    /**
     * Sets the font size of the heading label text.
     * @param fontSize Font size for the heading label text.
     * @throws InvalidInputException This exception shall be thrown if an input parameter is invalid.
     */
    public void setHeadingTextFontSize(int fontSize) throws InvalidInputException{
        this.setTextFontSize(fontSize);
    }

    /**
     * Returns the colour of the heading label text.
     * @return The getHeadingTextColor method returns the hex color code of the heading label text as a string.
     */
    public String getHeadingTextColor(){
        return this.getTextColor();
    }

    /**
     * Returns the font type of the heading label text.
     * @return The getHeadingTextFontName method shall return the font type of the heading label text.
     */
    public String getHeadingTextFontName(){
        return this.getTextFontName();
    }

    /**
     * Returns the font size of the heading label text.
     * @return The getHeadingTextFontSize method shall return the font size of the heading label text.
     */
    public int getHeadingTextFontSize(){
        return this.getTextFontSize();
    }
}
