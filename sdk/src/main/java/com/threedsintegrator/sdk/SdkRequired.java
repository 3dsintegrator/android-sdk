package com.threedsintegrator.sdk;

import java.security.PublicKey;

public class SdkRequired {

    public String sdkAppID;
    public PublicKeyClass sdkEphemPubKey;
    public String sdkReferenceNumber;
    public String sdkMaxTimeout;
    public String sdkEncData;
    public String sdkTransID;

    public SdkRequired(String sdkAppID, PublicKeyClass sdkEphemPubKey, String sdkReferenceNumber, String sdkMaxTimeout, String sdkEncData, String sdkTransID) {
        this.sdkAppID = sdkAppID;
        this.sdkEphemPubKey = sdkEphemPubKey;
        this.sdkReferenceNumber = sdkReferenceNumber;
        this.sdkMaxTimeout = sdkMaxTimeout;
        this.sdkEncData = sdkEncData;
        this.sdkTransID = sdkTransID;
    }

}
