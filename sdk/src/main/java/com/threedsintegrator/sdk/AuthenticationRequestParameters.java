package com.threedsintegrator.sdk;

import android.util.Log;

import java.security.KeyPair;
import java.security.PublicKey;

//Page 84
public class AuthenticationRequestParameters {
    private String sdkTransactionID;
    private String encryptedDeviceData;
    private String sdkAppID;
    private String sdkReferenceNumber;
    private String sdkEphemeralPublicKey;
    private String messageVersion;

    public AuthenticationRequestParameters(String sdkTransactionID, String deviceData, String sdkEphemeralPublicKey, String sdkAppID, String sdkReferenceNumber, String messageVersion, KeyPair keyPair) throws InvalidInputException {
        this.sdkTransactionID = sdkTransactionID;
        TransactionUtils transactionUtils = new TransactionUtils();
        transactionUtils.setReceiverPublicKey(keyPair.getPublic());
        this.encryptedDeviceData = transactionUtils.encrypt(deviceData);
//        Log.i(ThreeDS.MY_TAG, "Device information: " + deviceData);

        this.sdkAppID = sdkAppID;
        this.sdkReferenceNumber = sdkReferenceNumber;
        this.sdkEphemeralPublicKey = sdkEphemeralPublicKey;
        this.messageVersion = messageVersion;
    }

    /**
     * The createTransaction method generates a Transaction ID in UUID format
     * and the getSDKTransactionID method returns the Transaction ID as a String
     * @return String
     */
    public String getSdkTransactionID() {
        return this.sdkTransactionID;
    }

    /**
     * The getSDKAppID method shall return the SDK App ID. The 3DS SDK uses a
     * secure random function to generate the App ID in UUID format.
     * This ID is unique and is generated during installation and update of
     * the 3DS Requestor App on the Cardholder’s device.
     * @return String
     */
    public String getSdkAppID() {
        return this.sdkAppID;
    }

    /**
     * This method returns the SDK Reference Number as a string.
     * @return String
     */
    public String getSdkReferenceNumber() {
//        return BuildConfig.SDK_REF_NUMBER;
        return this.sdkReferenceNumber;
    }

    /**
     * The getSDKEphemeralPublicKey method shall return the SDK Ephemeral Public Key.
     * An ephemeral key pair is used to establish a secure session between the 3DS SDK
     * and the ACS. During each transaction, the createTransaction method generates a
     * fresh ephemeral key pair and the getSDKEphemeralPublicKey method returns the
     * public key component of the same as a String representation of a JWK object.
     * @return String
     */
    public String getSdkEphemeralPublicKey() {
        return this.sdkEphemeralPublicKey;
    }

    /**
     * The SDK receives the protocol version as a parameter in the createTransaction
     * method and determines whether it supports the version.
     * If the SDK does not receive the protocol version as a parameter in the createTransaction
     * method, then it returns the latest version that it supports.
     * @return String
     */
    public String getMessageVersion() {
        return this.messageVersion;
    }

    /**
     * This method returns the encrypted device data as a JWE string.
     * @return String
     */
    public String getEncryptedDeviceData() {
        return encryptedDeviceData;
    }
}
