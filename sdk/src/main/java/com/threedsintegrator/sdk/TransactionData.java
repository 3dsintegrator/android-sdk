package com.threedsintegrator.sdk;

import com.google.gson.Gson;

public class TransactionData {
    public String clientTransactionId;
    public Double amount;
    public String pan;
    public String month;
    public String year;
    public String cardHolderName;
    public String email;

    public AccountInfo accountInfo;
    public PhoneInfo workPhone;
    public PhoneInfo homePhone;
    public PhoneInfo mobilePhone;
    public AddressInfo shipping;
    public AddressInfo billing;
    public MerchantRiskIndicator merchantRiskIndicator;
    public SdkRequired sdk;
    public String threeDSRequestorURL;

    public TransactionData(String clientTransactionId, Double amount, String pan, String month, String year) {
        this.clientTransactionId = clientTransactionId;
        this.amount = amount;
        this.pan = pan;
        this.month = month;
        this.year = year;
    }

    public String convertToJson(){
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public void setClientTransactionId(String clientTransactionId) {
        this.clientTransactionId = clientTransactionId;
    }

    public String getClientTransactionId() {
        return clientTransactionId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public AccountInfo getAccountInfo() {
        return accountInfo;
    }

    public void setAccountInfo(AccountInfo accountInfo) {
        this.accountInfo = accountInfo;
    }

    public PhoneInfo getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(PhoneInfo workPhone) {
        this.workPhone = workPhone;
    }

    public PhoneInfo getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(PhoneInfo homePhone) {
        this.homePhone = homePhone;
    }

    public PhoneInfo getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(PhoneInfo mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public AddressInfo getShipping() {
        return shipping;
    }

    public void setShipping(AddressInfo shipping) {
        this.shipping = shipping;
    }

    public AddressInfo getBilling() {
        return billing;
    }

    public void setBilling(AddressInfo billing) {
        this.billing = billing;
    }

    public MerchantRiskIndicator getMerchantRiskIndicator() {
        return merchantRiskIndicator;
    }

    public void setMerchantRiskIndicator(MerchantRiskIndicator merchantRiskIndicator) {
        this.merchantRiskIndicator = merchantRiskIndicator;
    }

    public SdkRequired getSdk() {
        return sdk;
    }

    public String getThreeDSRequestorURL() {
        return threeDSRequestorURL;
    }

    public void setThreeDSRequestorURL(String threeDSRequestorURL) {
        this.threeDSRequestorURL = threeDSRequestorURL;
    }

    public void setSdk(SdkRequired sdk) {
        this.sdk = sdk;
    }
}
