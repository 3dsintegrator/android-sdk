package com.threedsintegrator.sdk;

//Page 63
/**
 * The ButtonCustomization class shall provide methods for the 3DS Requestor App
 * to pass button customization parameters to the 3DS SDK. This class shall extend
 * the Customization class.
 */
public class ButtonCustomization extends Customization {

    private String backgroundColor;
    private int cornerRadius;

    /**
     * Sets the background colour of the button.
     * @param hexColorCode Colour code in Hex format.
     * @throws InvalidInputException This exception shall be thrown if an input parameter is invalid.
     */
    public void setBackgroundColor(String hexColorCode) throws InvalidInputException{
        HexValidator hexValidator = new HexValidator();
        if (hexValidator.validate(hexColorCode)){
            this.backgroundColor = hexColorCode;
        }
        else {
            throw new InvalidInputException("Button backgroundColor code is not Hex format");
        }
    }

    /**
     * Sets the radius of the button corners.
     * @param cornerRadius Radius (integer value) for the button corners.
     * @throws InvalidInputException This exception shall be thrown if an input parameter is invalid.
     */
    public void setCornerRadius(int cornerRadius) throws InvalidInputException{
        try {
            this.cornerRadius = cornerRadius;
        }
        catch (Exception e){
            throw new InvalidInputException("Button cornerRadius input parameter is invalid.");
        }
    }

    /**
     * Returns the background colour of the button.
     * @return The getBackgroundColor method returns the background colour code (as a string) of the button.
     */
    public String getBackgroundColor(){
        return this.backgroundColor;
    }

    /**
     * Returns the radius of the button corners.
     * @return The getCornerRadius method returns the radius (as an integer) of the button corners.
     */
    public int getCornerRadius(){
        return this.cornerRadius;
    }
}
