package com.threedsintegrator.sdk;

//Pg 74
/**
 * The TextBoxCustomization class shall provide methods for the 3DS Requestor App
 * to pass text box customization parameters to the 3DS SDK.
 */
public class TextBoxCustomization extends Customization {
    private int borderWidth;
    private String borderColor;
    private int cornerRadius;

    public void setBorderWidth(int borderWidth){
        try {
            this.borderWidth = borderWidth;
        }
        catch (Exception e){
            throw new InvalidInputException("TextBox borderWidth input parameter is invalid.");
        }
    }

    public int getBorderWidth(){
        return this.borderWidth;
    }

    public void setBorderColor(String borderColor){
        try {
            this.borderColor = borderColor;
        }
        catch (Exception e){
            throw new InvalidInputException("TextBox borderColor input parameter is invalid.");
        }
    }

    public String getBorderColor(){
        return this.borderColor;
    }

    public void setCornerRadius(int cornerRadius){
        try {
            this.cornerRadius = cornerRadius;
        }
        catch (Exception e){
            throw new InvalidInputException("TextBox cornerRadius input parameter is invalid.");
        }
    }

    public int getCornerRadius(){
        return this.cornerRadius;
    }
}
