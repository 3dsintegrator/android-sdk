package com.threedsintegrator.sdk;

//Page 102
public class SDKNotInitializedException extends RuntimeException {
    public SDKNotInitializedException(String message) {
        super(message);
    }
}
