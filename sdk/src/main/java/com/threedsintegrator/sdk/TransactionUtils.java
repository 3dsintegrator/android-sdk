package com.threedsintegrator.sdk;

import android.util.Log;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.ECDSAVerifier;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.ECKey;
import com.nimbusds.jose.util.Base64;
import com.nimbusds.jose.util.X509CertUtils;

import net.minidev.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.*;
import java.security.interfaces.RSAPublicKey;
import java.text.ParseException;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyAgreement;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class TransactionUtils {

    private PublicKey publicKey;
    private KeyAgreement keyAgreement;
    private byte[] sharedSecret;

    private String ALGORITHM = "AES";

    public TransactionUtils() {
        this.makeKeyExchangeParams();
    }

    private void makeKeyExchangeParams() {
        KeyPairGenerator keyPairGenerator = null;
        try {
            keyPairGenerator = KeyPairGenerator.getInstance("EC");
            keyPairGenerator.initialize(256);
            KeyPair keyPair = keyPairGenerator.generateKeyPair();
            this.publicKey = keyPair.getPublic();
            this.keyAgreement = KeyAgreement.getInstance("ECDH");
            this.keyAgreement.init(keyPair.getPrivate());

        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
        }
    }

    public void setReceiverPublicKey(PublicKey publicKey) {
        try {
            this.keyAgreement.doPhase(publicKey, true);
            this.sharedSecret = this.keyAgreement.generateSecret();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
    }

    protected Key generateKey() {
        return new SecretKeySpec(this.sharedSecret, this.ALGORITHM);
    }

    public String encrypt(String msg) {
        try {
            Key key = generateKey();
            Cipher c = Cipher.getInstance(ALGORITHM);
            c.init(Cipher.ENCRYPT_MODE, key);
            byte[] encVal = c.doFinal(msg.getBytes());
            return  android.util.Base64.encodeToString(encVal, android.util.Base64.DEFAULT);
        } catch (BadPaddingException | InvalidKeyException | NoSuchPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return msg;
    }

    public String decrypt(String encryptedData) {
        try {
            Key key = generateKey();
            Cipher c = Cipher.getInstance(ALGORITHM);
            c.init(Cipher.DECRYPT_MODE, key);
            byte[] decodedValue = android.util.Base64.decode(encryptedData,android.util.Base64.DEFAULT);
            byte[] decValue = c.doFinal(decodedValue);
            return new String(decValue);
        } catch (BadPaddingException | InvalidKeyException | NoSuchPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return encryptedData;
    }

    public PublicKey getPublicKey() {
        return this.publicKey;
    }

    public void setPublicKey(PublicKey publicKey) {
        Log.i(ThreeDS.MY_TAG,"Public key: "+publicKey.toString());
        this.publicKey = publicKey;
    }

    public static JSONObject jsonData(ChallengeParameters challengeParameters, String messageVersion, String sdkTransactionID) {

        JSONObject obj = new JSONObject();

        obj.put("threeDSServerTransID", challengeParameters.serverTransactionID);
        obj.put("acsTransID", challengeParameters.acsTransactionID);
        obj.put("challengeCancel", "");
        obj.put("challengeDataEntry", "");
        obj.put("challengeHTMLDataEntry", "");
        obj.put("challengeWindowSize", "");
        obj.put("messageExtension", "");
        obj.put("messageType", "");
        obj.put("messageVersion", messageVersion);
        obj.put("oobContinue", "");
        obj.put("resendChallenge", "");
        obj.put("sdkTransID", sdkTransactionID);
        obj.put("sdkCounterStoA", "");

        return obj;
    }

    public static String jwsValidateSignatureAndReturnBody(String jws) {
        JWSObject jwsObject;
        try {
            jwsObject = JWSObject.parse(jws);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
            throw new RuntimeException("JWS parsing failed");

        }
        try {
            JWSVerifier verifier = null;
            JWSAlgorithm alg = jwsObject.getHeader().getAlgorithm();
            if (alg.equals(JWSAlgorithm.PS256) || alg.equals(JWSAlgorithm.RS256)) {
                List<Base64> x509CertChain =
                        jwsObject.getHeader().getX509CertChain();
                verifier = new RSASSAVerifier((RSAPublicKey)
                        X509CertUtils.parse(x509CertChain.get(0).decode()).getPublicKey());
            } else if (alg.equals(JWSAlgorithm.ES256)) {
                try {
                    verifier = new
                            ECDSAVerifier(ECKey.parse(jwsObject.getHeader().getJWK().toJSONString()));
                } catch (JOSEException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else {
                // unsupported algorithm
                throw new RuntimeException("Unsupported Algorithm");
            }
            try {
                if (!jwsObject.verify(verifier)) {
                    throw new RuntimeException("JWS validation failed.");
                }
            } catch (JOSEException e) {
                e.printStackTrace();
            } catch (Exception e) {
                throw new RuntimeException("JWS validation failed.");
            }
        } finally {
            return jwsObject.getPayload().toString();

        }
    }

    public static String sendCReq(String targetURL, String requestBody) {
        HttpURLConnection connection = null;

        try {
            //Create connection
            URL url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            //set request headers
            connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            connection.setRequestProperty("Content-Length", Integer.toString(requestBody.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setReadTimeout(20000);
            connection.setConnectTimeout(20000);

            //Send request
            OutputStream outputStream = connection.getOutputStream();
            BufferedWriter writer = new BufferedWriter( new OutputStreamWriter(outputStream));
            writer.write(requestBody);
            writer.flush();
            writer.close();
            outputStream.close();

            int responseCode=connection.getResponseCode();
            Log.i(ThreeDS.MY_TAG,Integer.toString(responseCode));

            DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream());
            wr.writeBytes(requestBody);
            wr.close();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
            String line;
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            return response.toString();
        } catch (Exception e) {
            Log.i(ThreeDS.MY_TAG, e.getMessage());
            return null;
        }
        finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

//        public static SecretKey generateECDHSecret(ECPublicKey pub, ECPrivateKey priv, String sdkReferenceId) {
//        try {
//            SecretKey z = ECDH.deriveSharedSecret(pub, priv, null);
//            ConcatKDF kdf = new ConcatKDF("SHA-256");
//            return kdf.deriveKey(
//                    z,
//                    256,
//                    ConcatKDF.encodeStringData(null), ConcatKDF.encodeDataWithLength((Base64URL) null), ConcatKDF.encodeDataWithLength(Base64URL.encode(sdkReferenceId)), ConcatKDF.encodeIntData(256),
//                    ConcatKDF.encodeNoData());
//        } catch (Exception e1) {
//            Log.i("failed to decrypt", e1.toString());
//        }
//        throw new RuntimeException();
//    }
//
//    public static byte[] encrypt(JSONObject challengeRequest, SecretKey secretKey, byte sdkCounterStoA) {
//        try {
//            final byte counter = sdkCounterStoA;
//            JWEHeader header = new JWEHeader.Builder(JWEAlgorithm.DIR, EncryptionMethod.A128GCM)
//                    .keyID((String) challengeRequest.get(ProtocolConstants.ACSTransactionID))
//                    .build();
//            challengeRequest.put(ProtocolConstants.SDKCounterStoA, String.format("%03d", counter));
//
//            JWEObject jweObject = new JWEObject(header, new Payload(challengeRequest.toString()));
//            jweObject.encrypt(new TransactionEncrypter(Arrays.copyOfRange(secretKey.getEncoded(), 0, 16), counter));
//
//            byte[] bytes = jweObject.serialize().getBytes();
//
//            sdkCounterStoA++;
//
//            if (sdkCounterStoA == 0) {
//                throw new RuntimeException("SdkCounterStoA zero");
//            }
//
//            return bytes;
//        } catch (Exception e) {
//            Log.e("Encryption Error", "JWE failed");
//        }
//        return challengeRequest.toString().getBytes();
//    }
//
//
//    public static JSONObject decrypt(String message, SecretKey secretKey, byte sdkCounterAtoS) {
//        try {
//            byte[] key = secretKey.getEncoded();
//            JWEObject jweObject = JWEObject.parse(message);
//            EncryptionMethod jweMethod = jweObject.getHeader().getEncryptionMethod();
//            if (jweMethod == EncryptionMethod.A128GCM) {
//                key = Arrays.copyOfRange(key, key.length - 16, key.length);
//            }
//            jweObject.decrypt(new DirectDecrypter(key));
//            JSONObject challengeResponse = jweObject.getPayload().toJSONObject();
//
//            byte acsCounterAtoS = Byte.valueOf(challengeResponse.get(ProtocolConstants.ACSCounterAtoS).toString());
//            if (sdkCounterAtoS != acsCounterAtoS) {
//                throw new RuntimeException("counters (" + sdkCounterAtoS + "/" + acsCounterAtoS + ")");
//            }
//            sdkCounterAtoS++;
//            if (sdkCounterAtoS == 0) {
//                throw new RuntimeException("SDKCounterAtoS zero");
//            }
//            return challengeResponse;
//        } catch (Exception e) {
//            Log.e("failed to decryptC", e.toString());
//        }
//        throw new RuntimeException();
//    }


}
