package com.threedsintegrator.sdk;

//Page 103
public class SDKRuntimeException extends RuntimeException{
    private String errorCode;

    public SDKRuntimeException(String message, String errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }
}
