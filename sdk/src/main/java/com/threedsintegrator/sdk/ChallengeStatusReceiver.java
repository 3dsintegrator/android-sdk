package com.threedsintegrator.sdk;

//Pg 45
public interface ChallengeStatusReceiver {
    /**
     * Called when the challenge process (that is, the transaction) is completed.
     * When a transaction is completed, a transaction status shall be available.
     * @param e Information about completion of the challenge process.
     */
    public void completed (CompletionEvent e);
    /**
     * The cancelled method shall be called when the Cardholder selects the
     * option to cancel the transaction on the challenge screen.
     */
    public void cancelled ();
    /**
     * The timedout method shall be called when the challenge process reaches or
     * exceeds the timeout specified during the doChallenge call on the 3DS SDK.
     * On timeout, the SDK shall make a best effort to stop the challenge flow as
     * soon as possible.
     */
    public void timedout ();
    /**
     * The protocolError method shall be called when the 3DS SDK receives such an
     * error message. The 3DS SDK sends the error code and details from this error
     * message as part of the notification to the 3DS Requestor App.
     * @param e Error code and details
     */
    public void protocolError(ProtocolErrorEvent e);
    /**
     * The runtimeError method shall be called when the 3DS SDK encounters errors
     * during the challenge process.
     * @param e Error code and details.
     */
    public void runtimeError(RuntimeErrorEvent e);
}
