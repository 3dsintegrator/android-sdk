package com.threedsintegrator.sdk;

public class AddressInfo {
    public String firstName;
    public String lastName;
    public String line1;
    public String line2;
    public String line3;
    public String city;
    public String postCode;
    public String state;
    public String country;

    public AddressInfo(String firstName, String lastName, String line1, String line2, String line3, String city, String postCode, String state, String country) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.line1 = line1;
        this.line2 = line2;
        this.line3 = line3;
        this.city = city;
        this.postCode = postCode;
        this.state = state;
        this.country = country;
    }
}
