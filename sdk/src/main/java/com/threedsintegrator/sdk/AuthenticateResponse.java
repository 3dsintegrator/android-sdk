package com.threedsintegrator.sdk;

import com.google.gson.annotations.SerializedName;

public class AuthenticateResponse {


    @SerializedName("authenticationValue")
    private String authenticationValue;

    @SerializedName("eci")
    private String eci;

    @SerializedName("status")
    private String status;

    @SerializedName("protocolVersion")
    private String protocolVersion;

    @SerializedName("dsTransId")
    private String dsTransId;

    @SerializedName("acsTransId")
    private String acsTransId;

    @SerializedName("cardToken")
    private String cardToken;

    @SerializedName("error")
    private String error;

    public String getAuthenticationValue() {
        return authenticationValue;
    }

    public String getEci() {
        return eci;
    }

    public String getStatus() {
        return status;
    }

    public String getProtocolVersion() {
        return protocolVersion;
    }

    public String getDsTransId() {
        return dsTransId;
    }

    public String getAcsTransId() {
        return acsTransId;
    }

    public String getCardToken() {
        return cardToken;
    }

    public String getError() {
        return error;
    }
}
