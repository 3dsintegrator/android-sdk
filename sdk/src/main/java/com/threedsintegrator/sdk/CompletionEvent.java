package com.threedsintegrator.sdk;

//Page 91

/**
 * The CompletionEvent class shall hold data about completion of the challenge process.
 */
public class CompletionEvent {
    private String sdkTransactionID;
    private String transactionStatus;

    public CompletionEvent(String sdkTransactionID, String transactionStatus) {
        this.sdkTransactionID = sdkTransactionID;
        this.transactionStatus = transactionStatus;
    }

    public String getSdkTransactionID() {
        return sdkTransactionID;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }
}
