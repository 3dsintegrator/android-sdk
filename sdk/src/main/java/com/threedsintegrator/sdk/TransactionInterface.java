package com.threedsintegrator.sdk;

import android.app.Activity;
import android.widget.ProgressBar;

//page 46
public interface TransactionInterface {
    /**
     * @return Returns device and 3DS SDK information to the 3DS Requestor App.
     * @throws SDKRuntimeException This exception shall be thrown if an internal error is encountered by the 3DS SDK.
     */
    public AuthenticationRequestParameters getAuthenticationRequestParameters() throws SDKRuntimeException;
    /**
     * Initiates the challenge process.
     * @param currentActivity The Android activity instance that invoked doChallenge.
     * @param challengeParameters ACS details (contained in the ARes) required by the 3DS SDK to conduct the challenge process during the transaction
     * @param challengeStatusReceiver Callback object for notifying the 3DS Requestor App about the challenge status.
     * @param timeOut Timeout interval (in minutes) within which the challenge process must be completed.
     * @throws InvalidInputException This exception shall be thrown if an input parameter is invalid. A timeout interval of less than 5 minutes is also treated as invalid input.
     * @throws SDKRuntimeException This exception shall be thrown if an internal error is encountered by the 3DS SDK.
     */
    public void doChallenge(Activity currentActivity, ChallengeParameters challengeParameters, ChallengeStatusReceiver challengeStatusReceiver, int timeOut) throws InvalidInputException, SDKRuntimeException;
    /**
     * Returns an instance of Progress View (processing screen) that the 3DS Requestor App
     * uses.
     *
     * NOTE: ProgressDialog is deprecated. ProgressBar will be used instead.
     *
     * @param currentActivity An Android activity instance.
     * @return This method returns a ProgressDialog object.
     * @throws InvalidInputException This exception shall be thrown if an input parameter is invalid.
     * @throws SDKRuntimeException This exception shall be thrown if an internal error is encountered by the 3DS SDK.
     */
    public ProgressBar getProgressView(Activity currentActivity) throws InvalidInputException, SDKRuntimeException;
    /**
     * Cleans up resources that are held by the TransactionInterface object.
     */
    public void close();
}
