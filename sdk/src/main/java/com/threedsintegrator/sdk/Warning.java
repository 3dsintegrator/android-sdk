package com.threedsintegrator.sdk;

//Page 97

/**
 * The Warning class shall represent a warning that is produced by the 3DS SDK
 * while performing security checks during initialization.
 */
public class Warning {
    public static final String MY_TAG = "my_android_tag";

    private String id;
    private String message;
    private Severity severity;

    public enum Severity {LOW, MEDIUM, HIGH};

    public Warning(String id, String message, Severity severity){
        this.id = id;
        this.message = message;
        this.severity = severity;
    }

    public String getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public Severity getSeverity() {
        return severity;
    }
}
