package com.threedsintegrator.sdk;

public class AccountInfo {
    public String chAccAgeInd;
    public String chAccDate;
    public String chAccChangeInd;
    public String chAccChange;
    public String chAccPwChangeInd;
    public String chAccPwChange;
    public String shipAddressUsageInd;
    public String shipAddressUsage;
    public String txnActivityDay;
    public String txnActivityYear;
    public String provisionAttemptsDay;
    public String nbPurchaseAccount;
    public String suspiciousAccActivity;
    public String shipNameIndicator;
    public String paymentAccAge;
    public String paymentAccInd;

    public AccountInfo(String chAccAgeInd, String chAccDate, String chAccChangeInd, String chAccChange, String chAccPwChangeInd, String chAccPwChange, String shipAddressUsageInd, String shipAddressUsage, String txnActivityDay, String txnActivityYear, String provisionAttemptsDay, String nbPurchaseAccount, String suspiciousAccActivity, String shipNameIndicator, String paymentAccAge, String paymentAccInd) {
        this.chAccAgeInd = chAccAgeInd;
        this.chAccDate = chAccDate;
        this.chAccChangeInd = chAccChangeInd;
        this.chAccChange = chAccChange;
        this.chAccPwChangeInd = chAccPwChangeInd;
        this.chAccPwChange = chAccPwChange;
        this.shipAddressUsageInd = shipAddressUsageInd;
        this.shipAddressUsage = shipAddressUsage;
        this.txnActivityDay = txnActivityDay;
        this.txnActivityYear = txnActivityYear;
        this.provisionAttemptsDay = provisionAttemptsDay;
        this.nbPurchaseAccount = nbPurchaseAccount;
        this.suspiciousAccActivity = suspiciousAccActivity;
        this.shipNameIndicator = shipNameIndicator;
        this.paymentAccAge = paymentAccAge;
        this.paymentAccInd = paymentAccInd;
    }
}
