package com.threedsintegrator.sdk;

import android.util.Log;

public class LogDebug {
    private static final String TAG = "TEST_TAG";

    public static void d(String message) {
        Log.d(TAG, message);
    }
}
