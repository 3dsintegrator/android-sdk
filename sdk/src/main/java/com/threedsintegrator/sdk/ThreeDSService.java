package com.threedsintegrator.sdk;

import android.content.Context;

import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public interface ThreeDSService {
    public void initialize(Context applicationContext, ConfigParameters configParameters, String locale, UiCustomization uiCustomization);
    public List<Warning> getWarnings();
    public Transaction createTransaction(String directoryServerID, String messageVersion) throws InvalidInputException, InvalidAlgorithmParameterException, NoSuchAlgorithmException;
    public String getSDKVersion() throws SDKNotInitializedException, SDKRuntimeException;
    public void cleanup(Context applicationContext)throws SDKNotInitializedException;
}
