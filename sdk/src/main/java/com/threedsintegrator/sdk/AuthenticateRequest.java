package com.threedsintegrator.sdk;

import com.google.gson.annotations.SerializedName;

public class AuthenticateRequest {

    @SerializedName("body")
    private String body;

    public String getBody() {
        return body;
    }
}
