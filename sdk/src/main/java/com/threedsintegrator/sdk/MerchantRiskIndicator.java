package com.threedsintegrator.sdk;

public class MerchantRiskIndicator {
    public String shipIndicator;
    public String reorderItemsInd;
    public String deliveryTimeframe;
    public String deliveryEmailAddress;
    public String giftCardCurr;
    public String giftCardCount;
    public String giftCardAmount;
    public String preOrderPurchaseInd;
    public String preOrderDate;

    public MerchantRiskIndicator(String shipIndicator, String reorderItemsInd, String deliveryTimeframe, String deliveryEmailAddress, String giftCardCurr, String giftCardCount, String giftCardAmount, String preOrderPurchaseInd, String preOrderDate) {
        this.shipIndicator = shipIndicator;
        this.reorderItemsInd = reorderItemsInd;
        this.deliveryTimeframe = deliveryTimeframe;
        this.deliveryEmailAddress = deliveryEmailAddress;
        this.giftCardCurr = giftCardCurr;
        this.giftCardCount = giftCardCount;
        this.giftCardAmount = giftCardAmount;
        this.preOrderPurchaseInd = preOrderPurchaseInd;
        this.preOrderDate = preOrderDate;
    }
}
