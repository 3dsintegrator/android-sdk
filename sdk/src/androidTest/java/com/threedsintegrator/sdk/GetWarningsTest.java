package com.threedsintegrator.sdk;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.test.InstrumentationRegistry;
import android.util.Log;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class GetWarningsTest {
    private String locale;
    private Context applicationContext;
    private String[]  warnings;
    private Activity activity;
    private ConfigParameters configParameters;
    private UiCustomization uiCustomization;
    private TransactionData transactionData;

    @Before
    public void setUp() throws Exception {
        this.applicationContext = InstrumentationRegistry.getTargetContext();
        setFinalStatic(Build.VERSION.class.getField("SDK_INT"), 20);
    }

    static void setFinalStatic(Field field, Object newValue) throws Exception {
        field.setAccessible(true);

        Field modifiersField = Field.class.getDeclaredField("accessFlags");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

        field.set(null, newValue);
    }

    @Test
    public void getWarnings(){
        //add OS Version Warning
        Warning jailBrokenWarning = new Warning("SW01","The device is jailbroken.", Warning.Severity.HIGH);
        Warning emulatorWarning = new Warning("SW03","An emulator is being used to run the App.", Warning.Severity.HIGH);
        Warning debuggerWarning = new Warning("SW04","A debugger is attached to the App.", Warning.Severity.MEDIUM);
        Warning osVersionWarning = new Warning("SW05","The OS or the OS version is not supported.", Warning.Severity.HIGH);

        List<Warning> actualWarnings;

        ThreeDS threeDS = new ThreeDS();
        this.configParameters = new ConfigParameters();
        threeDS.initialize(this.applicationContext, this.configParameters,this.locale,this.uiCustomization);
        actualWarnings = threeDS.getWarnings();

        //there should be at least one warning - the OS Version
        int actualSize = actualWarnings.size();
        assertTrue(actualSize >= 1);

        Log.i(ThreeDS.MY_TAG,"Actual Size is: "+actualSize);
        for (int i = 0; i < actualSize; i++) {
            Warning actualWarning = actualWarnings.get(i);
            if (actualWarning.getId().equals("SW05")){
                assertEquals(osVersionWarning.getMessage(),actualWarning.getMessage());
                assertEquals(osVersionWarning.getSeverity(),actualWarning.getSeverity());
            }
            else if(actualWarning.getId().equals("SW01")){
                assertEquals(jailBrokenWarning.getMessage(),actualWarning.getMessage());
                assertEquals(jailBrokenWarning.getSeverity(),actualWarning.getSeverity());
            }
            else if (actualWarning.getId().equals("SW03")){
                assertEquals(emulatorWarning.getMessage(),actualWarning.getMessage());
                assertEquals(emulatorWarning.getSeverity(),actualWarning.getSeverity());
            }
            else if (actualWarning.getId().equals("SW04")){
                assertEquals(debuggerWarning.getMessage(),actualWarning.getMessage());
                assertEquals(debuggerWarning.getSeverity(),actualWarning.getSeverity());
            }
        }

    }
}