package com.threedsintegrator.sdk;


import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class ConfigParametersTest {

    @Test(expected = InvalidInputException.class)
    public void addParamParamNameNull() {
        ConfigParameters configParameters = new ConfigParameters();
        configParameters.addParam(ConfigParameters.DEFAULT_GROUP,null,"red");
    }

    @Test(expected = InvalidInputException.class)
    public void addParamParamValueNull() {
        ConfigParameters configParameters = new ConfigParameters();
        configParameters.addParam(ConfigParameters.DEFAULT_GROUP,"Color",null);
    }

    @Test(expected = InvalidInputException.class)
    public void addParamGroupValueAlreadyExist() {
        ConfigParameters configParameters = new ConfigParameters();
        configParameters.addParam(ConfigParameters.DEFAULT_GROUP,"Color","red");
        configParameters.addParam(ConfigParameters.DEFAULT_GROUP,"Color","red");
    }

    @Test
    public void getParamValueSuccess() {
        ConfigParameters configParameters = new ConfigParameters();
        configParameters.addParam(ConfigParameters.DEFAULT_GROUP,"Color","red");
        String value = configParameters.getParamValue(ConfigParameters.DEFAULT_GROUP,"Color");
        assertEquals("red",value);
    }

    @Test(expected = InvalidInputException.class)
    public void getParamValueFail() {
        ConfigParameters configParameters = new ConfigParameters();
        configParameters.addParam(ConfigParameters.DEFAULT_GROUP,"Color","red");
        String value = configParameters.getParamValue(ConfigParameters.DEFAULT_GROUP,null);
    }

    @Test
    public void removeParamSuccess() {
        ConfigParameters configParameters = new ConfigParameters();
        //Add Param
        configParameters.addParam(ConfigParameters.DEFAULT_GROUP,"Color","red");
        //Remove Param
        String value = configParameters.removeParam(ConfigParameters.DEFAULT_GROUP,"Color");
        assertEquals("red",value);
        //check that param is really gone
        String getValue = configParameters.getParamValue(ConfigParameters.DEFAULT_GROUP,"Color");
        assertNull(getValue);
    }

    @Test(expected = InvalidInputException.class)
    public void removeParamFail() {
        ConfigParameters configParameters = new ConfigParameters();
        configParameters.addParam(ConfigParameters.DEFAULT_GROUP,"Color","red");
        String value = configParameters.removeParam(ConfigParameters.DEFAULT_GROUP,null);
    }
}