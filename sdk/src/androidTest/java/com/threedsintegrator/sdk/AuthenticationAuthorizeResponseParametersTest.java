package com.threedsintegrator.sdk;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.test.InstrumentationRegistry;
import android.util.Log;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public class AuthenticationAuthorizeResponseParametersTest {

    private String locale = "";
    private Context applicationContext;
    private Activity activity;
    private ConfigParameters configParameters = new ConfigParameters();
    private UiCustomization uiCustomization = new UiCustomization();
    private TransactionData transactionData;
    private ThreeDS threeDS;

    @Before
    public void setUp() throws Exception {
        this.applicationContext = InstrumentationRegistry.getTargetContext();
        setFinalStatic(Build.VERSION.class.getField("SDK_INT"), 28);

        threeDS = new ThreeDS();
        threeDS.initialize(this.applicationContext, configParameters, locale, uiCustomization);

    }

    static void setFinalStatic(Field field, Object newValue) throws Exception {
        field.setAccessible(true);

        Field modifiersField = Field.class.getDeclaredField("accessFlags");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

        field.set(null, newValue);
    }

    @After
    public void tearDown() throws Exception {
    }

//    @Test
//    public void getSdkTransactionID() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException {
//        Transaction transaction = threeDS.createTransaction("001", "001");
//
//        AuthenticationRequestParameters auth = transaction.getAuthenticationRequestParameters();
//
//        String transactionID = auth.getSdkTransactionID();
//
//        Log.i("TRANSACTION_ID", transactionID);
//
//        Assert.assertNotNull(transactionID);
//        Assert.assertTrue(transactionID instanceof String);
//    }

//    @Test
//    public void getDeviceData() throws InvalidKeySpecException, NoSuchAlgorithmException, InvalidAlgorithmParameterException {
//
//        Transaction transaction = threeDS.createTransaction("001", "001");
//
//        AuthenticationRequestParameters auth = transaction.getAuthenticationRequestParameters();
//
//        String deviceData = auth.getDeviceData();
//
//        Log.i("Device_Data", deviceData);
//
//        Assert.assertNotNull(deviceData);
//        Assert.assertTrue(deviceData instanceof String);
//    }

//    @Test
//    public void getSdkEphemeralPublicKey() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException {
//
//        Transaction transaction = threeDS.createTransaction("001", "001");
//
//        AuthenticationRequestParameters auth = transaction.getAuthenticationRequestParameters();
//
//        String key = auth.getSdkEphemeralPublicKey();
//
//        Log.i("EPKEY", key);
//
//        Assert.assertNotNull(key);
//        Assert.assertTrue(key instanceof String);
//
//    }

//    @Test
//    public void getSdkAppID() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException {
//        Transaction transaction = threeDS.createTransaction("001", "001");
//
//        AuthenticationRequestParameters auth = transaction.getAuthenticationRequestParameters();
//
//        String appID = auth.getSdkAppID();
//
//        Log.i("APP_ID", appID);
//
//        Assert.assertNotNull(appID);
//        Assert.assertTrue(appID instanceof String);
//    }

//    @Test
//    public void getSdkReferenceNumber() {
//        Transaction transaction = null;
//        try {
//            transaction = threeDS.createTransaction("001", "001");
//        } catch (InvalidAlgorithmParameterException e) {
//            e.printStackTrace();
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }
//
//        AuthenticationRequestParameters auth = transaction.getAuthenticationRequestParameters();
//
//        String sdkRefNumber = auth.getSdkReferenceNumber();
//
//        String expectedRefNumber = BuildConfig.SDK_REF_NUMBER;
//
//        Log.i("REF_NUMBER", sdkRefNumber);
//
//        Assert.assertNotNull(sdkRefNumber);
//        Assert.assertEquals(sdkRefNumber, expectedRefNumber);
//
//    }

//    @Test
//    public void getMessageVersion() {
//        Transaction transaction = null;
//        try {
//            transaction = threeDS.createTransaction("001", "00043231");
//        } catch (InvalidAlgorithmParameterException e) {
//            e.printStackTrace();
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }
//
//        AuthenticationRequestParameters auth = transaction.getAuthenticationRequestParameters();
//
//        String messageVersion = auth.getMessageVersion();
//
//        String expectedMessageVersion = "00043231";
//
//        Log.i("MESSAGE_VERSION", messageVersion);
//
//        Assert.assertNotNull(messageVersion);
//        Assert.assertEquals(messageVersion, expectedMessageVersion);
//    }
}