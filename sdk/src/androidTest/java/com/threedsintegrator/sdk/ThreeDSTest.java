package com.threedsintegrator.sdk;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.test.InstrumentationRegistry;
import android.util.Log;

import net.minidev.json.JSONArray;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ThreeDSTest {
    private String locale;
    private Context applicationContext;
    private Activity activity;
    private ConfigParameters configParameters = new ConfigParameters();
    private UiCustomization uiCustomization = new UiCustomization();
    private TransactionData transactionData;
    public static final String MY_TAG = "my_android_tag";
    private List<DeviceData> availableDeviceData;
    private List<DeviceData> unavailableDeviceData;


    @Before
    public void setUp() throws Exception {
        this.applicationContext = InstrumentationRegistry.getTargetContext();
        setFinalStatic(Build.VERSION.class.getField("SDK_INT"), 28);
    }

    static void setFinalStatic(Field field, Object newValue) throws Exception {
        field.setAccessible(true);

        Field modifiersField = Field.class.getDeclaredField("accessFlags");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

        field.set(null, newValue);
    }

    @Test
    public void initialize(){
        ConfigParameters expectedConfigParameters = new ConfigParameters();
        UiCustomization expectedUiCustomization = new UiCustomization();
        String expectedLocale = "test-locale";

        ThreeDS threeDS = new ThreeDS();
        threeDS.initialize(this.applicationContext,expectedConfigParameters,expectedLocale,expectedUiCustomization);

        ConfigParameters actualConfigParameters =  threeDS.getConfigParameters();
        String actualLocale = threeDS.getLocale();
        UiCustomization actualUiCustomization = threeDS.getUiCustomization();

        assertEquals(actualConfigParameters,expectedConfigParameters);
        assertEquals(actualLocale,expectedLocale);
        assertEquals(actualUiCustomization,expectedUiCustomization);
    }

    @Test
    public void isValidOperatingSystemVersion() {
        boolean output;
        List<Warning> warnings = new ArrayList<Warning>();

        ThreeDS threeDS = new ThreeDS();
        output = threeDS.isValidOperatingSystemVersion();

        assertTrue(output);
        //get warnings
        warnings = threeDS.getTestWarnings();
        Log.i(ThreeDS.MY_TAG,"Size is: "+warnings.size());
        assertEquals(0,warnings.size());

    }

    @Test
    public void sandboxModeTestUrl(){
        String expectedValue = "sandbox";
        String expectedUrl = "https://api-sandbox.3dsintegrator.com/v2/";
        this.configParameters.addParam(ConfigParameters.DEFAULT_GROUP,"mode","sandbox");

        ThreeDS threeDS = new ThreeDS();
        threeDS.initialize(this.applicationContext, this.configParameters,this.locale,this.uiCustomization);

        String actualUrl = threeDS.getBaseUrl();
        assertEquals(expectedUrl,actualUrl);

        String actualValue = threeDS.getConfigParameters().getParamValue(null,"mode");
        assertEquals(actualValue,expectedValue);
    }

    @Test
    public void sandboxModeLiveUrl(){
        String expectedValue = "production";
        String expectedUrl = "https://api.3dsintegrator.com";

        this.configParameters.addParam(ConfigParameters.DEFAULT_GROUP,"mode","production");
        ThreeDS threeDS = new ThreeDS();
        threeDS.initialize(this.applicationContext, this.configParameters,this.locale,this.uiCustomization);

        String actualUrl = threeDS.getBaseUrl();
        assertEquals(expectedUrl,actualUrl);

        String actualValue = threeDS.getConfigParameters().getParamValue(null,"mode");
        assertEquals(actualValue,expectedValue);
    }

    @Test
    public void verboseTest(){
        String expectedValue = "true";
        configParameters.addParam(ConfigParameters.DEFAULT_GROUP,"verbose","true");

        ThreeDS threeDS = new ThreeDS();
        threeDS.initialize(this.applicationContext, this.configParameters,this.locale,this.uiCustomization);

        String actualValue = threeDS.getConfigParameters().getParamValue(null,"verbose");
        assertEquals(expectedValue,actualValue);
    }

    @Test
    public void cleanup(){
        ThreeDS threeDS = new ThreeDS();
        threeDS.initialize(this.applicationContext, this.configParameters,this.locale,this.uiCustomization);

        threeDS.cleanup(this.applicationContext);

        Assert.assertTrue(threeDS.isCleaned);
    }

}