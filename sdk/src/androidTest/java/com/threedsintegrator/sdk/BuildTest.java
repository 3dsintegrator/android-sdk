package com.threedsintegrator.sdk;

public class BuildTest {
    public String FINGERPRINT = null;
    public String MODEL = null;
    public String MANUFACTURER = null;
    public String BRAND = null;
    public String PRODUCT = null;
    public String DEVICE = null;


    public BuildTest(String fingerPrint, String model, String manufacturer, String brand, String product, String device){
        this.FINGERPRINT = fingerPrint;
        this.MODEL = model;
        this.MANUFACTURER = manufacturer;
        this.BRAND = brand;
        this.PRODUCT = product;
        this.DEVICE = device;
    }

}