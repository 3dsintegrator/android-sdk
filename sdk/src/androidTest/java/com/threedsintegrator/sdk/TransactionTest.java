package com.threedsintegrator.sdk;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.KeyAgreement;

import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class TransactionTest {
    private PublicKey publicKey;
    private KeyAgreement keyAgreement;
    private byte[] sharedSecret;
    public static final String MY_TAG = "my_android_tag";

    /**
     * Test for generating fresh ephemeral key pair as described in Annex C in EMVCo_3DS_Spec_210_1017_0318
     */
    @Test
    public void generateECCKeyPair() {
        KeyPairGenerator keyPairGenerator = null;
        String publicKeyString;
        try {
            keyPairGenerator = KeyPairGenerator.getInstance("EC");
            keyPairGenerator.initialize(new ECGenParameterSpec("secp521r1"));
            KeyPair keyPair = keyPairGenerator.generateKeyPair();
            ECPublicKey publicKey = (ECPublicKey) keyPair.getPublic();
            ECPrivateKey privateKey = (ECPrivateKey) keyPair.getPrivate();

            publicKeyString = publicKey.toString();
            Log.i(MY_TAG, "Public Key: " + publicKeyString);

        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void encryptAndDecrypt() throws NoSuchAlgorithmException, InvalidKeySpecException {
        TransactionUtils transactionUtils = new TransactionUtils();
        String message = "hello world";

        //Create a public key for the DS
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("EC");
        kpg.initialize(256);
        KeyPair kp = kpg.generateKeyPair();

        Log.i(ThreeDS.MY_TAG,"ACS public key: "+kp.getPublic().toString());

        //set the public key
        transactionUtils.setReceiverPublicKey(kp.getPublic());
        //encrypt the message
        String encryptedMessage = transactionUtils.encrypt(message);

        Log.i(ThreeDS.MY_TAG,"Encrypted message: "+encryptedMessage);

        //now we decrypt
        String decryptedMessage = transactionUtils.decrypt(encryptedMessage);
        Log.i(ThreeDS.MY_TAG,"Decrypted message: "+decryptedMessage);

        assertEquals(message,decryptedMessage);
    }
}
