package com.threedsintegrator.sdk;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.test.InstrumentationRegistry;
import android.util.Log;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

//Run in debug mode
public class WarningsTest {
    private String locale;
    private Context applicationContext;
    private String[]  warnings;
    private List<Warning>  eWarnings;
    private Activity activity;
    private ConfigParameters configParameters = new ConfigParameters();
    private UiCustomization uiCustomization = new UiCustomization();
    private TransactionData transactionData;

    @Before
    public void setUp() throws Exception {
        this.applicationContext = InstrumentationRegistry.getTargetContext();
        eWarnings = new ArrayList<Warning>();
        setFinalStatic(Build.VERSION.class.getField("SDK_INT"), 20);
    }

    static void setFinalStatic(Field field, Object newValue) throws Exception {
        field.setAccessible(true);

        Field modifiersField = Field.class.getDeclaredField("accessFlags");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

        field.set(null, newValue);
    }

    @Test
    public void isNotValidOperatingSystemVersion() {
        boolean output;
        Warning expectedWarning = new Warning("SW05","The OS or the OS version is not supported.", Warning.Severity.HIGH);
        List<Warning> warnings = new ArrayList<Warning>();

        ThreeDS threeDS = new ThreeDS();
        output = threeDS.isValidOperatingSystemVersion();

        //returns false
        assertFalse(output);

        //get warnings
        warnings = threeDS.getTestWarnings();
        Log.i(ThreeDS.MY_TAG,"Size is: "+warnings.size());
        assertEquals(1,warnings.size());

        //compare expected vs actual
        Warning actualWarning = warnings.get(0);
        assertEquals(expectedWarning.getId(),actualWarning.getId());
        assertEquals(expectedWarning.getMessage(),actualWarning.getMessage());
        assertEquals(expectedWarning.getSeverity(),actualWarning.getSeverity());
    }

    //NOTE: Run test in debugger mode
    @Test
    public void isDebuggerConnected() {
        boolean output;
        Warning expectedWarning = new Warning("SW04","A debugger is attached to the App.", Warning.Severity.MEDIUM);
        List<Warning> warnings = new ArrayList<Warning>();

        ThreeDS threeDS = new ThreeDS();
//        threeDS.initialize(this.applicationContext,this.configParameters,this.locale,this.uiCustomization);
        output = threeDS.isDebuggerConnected();

        if (output){
            //get warnings
            warnings = threeDS.getTestWarnings();
            Log.i(ThreeDS.MY_TAG,"Size is: "+warnings.size());
            assertEquals(1,warnings.size());

            //compare expected vs actual
            Warning actualWarning = warnings.get(0);
            assertEquals(expectedWarning.getId(),actualWarning.getId());
            assertEquals(expectedWarning.getMessage(),actualWarning.getMessage());
            assertEquals(expectedWarning.getSeverity(),actualWarning.getSeverity());
        }
        else {
            //get warnings
            warnings = threeDS.getTestWarnings();
            Log.i(ThreeDS.MY_TAG,"Size is: "+warnings.size());
            assertEquals(0,warnings.size());
        }

    }

    @Test
    public void getSDKVersion() {
        String expectedValue = "1.0.0.0";
        ThreeDS threeDS = new ThreeDS();
        String actualValue = threeDS.getTestSDKVersion();

        assertEquals(actualValue,expectedValue);
    }

    @Test
    public void isJailBroken() {
        boolean output;
        Warning expectedWarning = new Warning("SW01","The device is jailbroken.", Warning.Severity.HIGH);
        List<Warning> warnings = new ArrayList<Warning>();

        ThreeDS threeDS = new ThreeDS();
        threeDS.initialize(this.applicationContext, this.configParameters,this.locale,this.uiCustomization);
        output = threeDS.isJailBroken();
        assertTrue(output);
    }

    public boolean isEmulatorTest(BuildTest buildTest) {
        boolean result;
        result = buildTest.FINGERPRINT.startsWith("generic")
                || buildTest.FINGERPRINT.startsWith("unknown")
                || buildTest.MODEL.contains("google_sdk")
                || buildTest.MODEL.contains("Emulator")
                || buildTest.MODEL.contains("Android SDK built for x86")
                || buildTest.MANUFACTURER.contains("Genymotion")
                || (buildTest.BRAND.startsWith("generic") && buildTest.DEVICE.startsWith("generic"))
                || "google_sdk".equals(buildTest.PRODUCT);

        if (result){
            Warning warning = new Warning("SW03","An emulator is being used to run the App.", Warning.Severity.HIGH);
            this.eWarnings.add(warning);
        }
        return result;
    }

    @Test
    public void isNotEmulator() {
        boolean output;

        //Create a BuildTest that has REAL device information
        String fingerprint = "google/angler/angler:8.1.0/OPM6.171019.030.K1/4947289:user/release-keys";
        String model = "Nexus 6P";
        String manufacturer = "Huawei";
        String brand = "google";
        String product = "angler";
        String device = "angler";
        BuildTest buildTest = new BuildTest(fingerprint,model,manufacturer,brand,product,device);

        output = isEmulatorTest(buildTest);
        assertFalse(output);

        //get warnings
        Log.i(ThreeDS.MY_TAG,"Size is: "+this.eWarnings.size());
        assertEquals(0,this.eWarnings.size());
    }

    @Test
    public void isEmulator() {
        boolean output;
        Warning expectedWarning = new Warning("SW03","An emulator is being used to run the App.", Warning.Severity.HIGH);

        //Create a BuildTest that has the EMULATOR device information
        String fingerprint = "Android/sdk_google_phone_x86/generic_x86:7.1.1/NYC/4252396:userdebug/test-keys";
        String model = "Android SDK built for x86";
        String manufacturer = "unknown";
        String brand = "Android";
        String product = "sdk_google_phone_x86";
        String device = "generic_x86";
        BuildTest buildTest = new BuildTest(fingerprint,model,manufacturer,brand,product,device);

        output = isEmulatorTest(buildTest);
        assertTrue(output);

        //get warnings
        Log.i(ThreeDS.MY_TAG,"Size is: "+eWarnings.size());
        assertEquals(1,eWarnings.size());

        //compare expected vs actual
        Warning actualWarning = eWarnings.get(0);
        assertEquals(expectedWarning.getId(),actualWarning.getId());
        assertEquals(expectedWarning.getMessage(),actualWarning.getMessage());
        assertEquals(expectedWarning.getSeverity(),actualWarning.getSeverity());
    }
}