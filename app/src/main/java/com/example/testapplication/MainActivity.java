package com.example.testapplication;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.threedsintegrator.sdk.ConfigParameters;
import com.threedsintegrator.sdk.ThreeDS;
import com.threedsintegrator.sdk.Transaction;
import com.threedsintegrator.sdk.TransactionData;
import com.threedsintegrator.sdk.UiCustomization;
import com.threedsintegrator.sdk.Warning;

import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.time.Year;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    Button submitButton, instructionButton;
    EditText creditCardNumber;
    EditText streetAddressOne;
    EditText streetAddressTwo;
    EditText city;
    EditText zip;
    Spinner expiryDateMonth;
    Spinner expiryDateYear;
    View view;
    String month;
    String year;
    String apiKey = "qI83kBQ49N9PmWiNCC8zNpb8Kxhb1FcI";
    String messageVersion = "2.1.0";
    String mode = "sandbox";
    String verbose = "true";

    ThreeDS threeDS = new ThreeDS();
    Transaction transaction;
    ConfigParameters configParameters = new ConfigParameters();
    String locale = "en-US";
    UiCustomization uiCustomization = new UiCustomization();

    private DatePickerDialog.OnDateSetListener dateSetListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        view = findViewById(R.id.myProgressButton);
        instructionButton = (Button)findViewById(R.id.instructionButton);
        creditCardNumber = (EditText)findViewById(R.id.creditCardNumber);
        streetAddressOne = (EditText)findViewById(R.id.streetAddressOne);
        streetAddressTwo = (EditText)findViewById(R.id.streetAddressTwo);
        city = (EditText)findViewById(R.id.city);
        zip = (EditText)findViewById(R.id.zip);
        expiryDateMonth = findViewById(R.id.expiryDateMonth);
        expiryDateYear = findViewById(R.id.expiryDateYear);

        instructionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialog();
            }
        });

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.months,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        expiryDateMonth.setAdapter(adapter);
        expiryDateMonth.setOnItemSelectedListener(this);

        List<String> years = new ArrayList<>();
        int year = Calendar.getInstance().get(Calendar.YEAR);
        //get next 10 years
        int i = 0;
        for (i=0;i<11;i++){
            years.add(String.valueOf(year+i).substring(2,4)) ;
        }
//        Log.i(threeDS.MY_TAG, "Years: "+years);
        ArrayAdapter<String> adapterYears = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,years);
        adapterYears.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        expiryDateYear.setAdapter(adapterYears);
        expiryDateYear.setOnItemSelectedListener(this);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //disable view to prevent multiple clicks
                View myView = findViewById(view.getId());
                myView.setEnabled(false);

                final ProgressButton progressButton = new ProgressButton(MainActivity.this, view);
                progressButton.buttonActivated();

                //performThreeDS();
                ThreeDSThread threeDSThread = new ThreeDSThread(MainActivity.this);
                threeDSThread.start();

//                if (threeDSThread.isAlive()) {
//                    //Toast.makeText(this,"Processing...",Toast.LENGTH_SHORT).show();
//                }

                try {
                    threeDSThread.join();

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressButton.buttonFinished();
                            Handler handler1 = new Handler();
                            handler1.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Intent intent = new Intent(MainActivity.this, ReportActivity.class);
                                    intent.putExtra("mode",mode);
                                    intent.putExtra("messageVersion",messageVersion);
                                    intent.putExtra("verbose",verbose);

                                    //Get Warnings
                                    List<Warning> warnings = threeDS.getWarnings();
                                    String warningCodes = "";
                                    for (Warning warning: warnings) {
                                        warningCodes += warning.getId()+", ";
                                    }
                                    intent.putExtra("warnings",warningCodes);

                                    //check authorization
                                    if (!threeDS.getAuthorization().equals("") && threeDS.getAuthorization().contains("Bearer")){
                                        intent.putExtra("authorization","Success");
                                    }
                                    else {
                                        intent.putExtra("authorization","Fail");
                                    }

                                    //check authentication
                                    if (threeDS.getAuthentication()){
                                        intent.putExtra("authentication","Success");
                                    }
                                    else {
                                        intent.putExtra("authentication","Fail");
                                    }

                                    //get error messages
                                    ArrayList<String> errorMessages = threeDS.getErrorMessages();
                                    String errors = "";
                                    for (String error: errorMessages) {
                                        errors += error+" | ";
                                    }
                                    intent.putExtra("errors",errors);
                                    threeDS.cleanup(MainActivity.this);
                                    //start new activity
                                    startActivity(intent);
                                }
                            },1000);

                        }
                    },3000);

                }
                catch (InterruptedException e){
                    Log.i(threeDS.MY_TAG, "Thread Exception: "+e.getMessage());
                }
            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (adapterView.getId()){
            case R.id.expiryDateMonth:
                month = adapterView.getItemAtPosition(i).toString();
                break;
            case R.id.expiryDateYear:
                year = adapterView.getItemAtPosition(i).toString();
                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public void openDialog(){
        InstructionDialog instructionDialog = new InstructionDialog();
        instructionDialog.show(getSupportFragmentManager(),"instruction dialog");
    }

    public void performThreeDS(){
        ThreeDSThread threeDSThread = new ThreeDSThread(this);
        threeDSThread.start();

//        if (threeDSThread.isAlive()) {
//            Toast.makeText(this,"Processing...",Toast.LENGTH_SHORT).show();
//        }

        try {
            threeDSThread.join();
//            Toast.makeText(this,"View logs.",Toast.LENGTH_LONG).show();
        }
        catch (InterruptedException e){
            Log.i(threeDS.MY_TAG, "Thread Exception: "+e.getMessage());
        }

    }

    class ThreeDSThread extends Thread {
        Activity activity;

        public ThreeDSThread(Activity activity) {
            this.activity = activity;
        }

        @Override
        public void run() {
            configParameters.addParam(null,"verbose",verbose);
            configParameters.addParam(null,"mode",mode);
            configParameters.addParam(null,"apiKey",apiKey);

            String cardNumber = creditCardNumber.getText().toString().trim();
            String submittedStreetAddressOne = streetAddressOne.getText().toString().trim();
            String submittedStreetAddressTwo = streetAddressTwo.getText().toString().trim();
            String submittedCity = city.getText().toString().trim();
            String submittedZip = zip.getText().toString().trim();

            Log.i(ThreeDS.MY_TAG, "Street Address 1: "+submittedStreetAddressOne);
            Log.i(ThreeDS.MY_TAG, "Street Address 2: "+submittedStreetAddressTwo);
            Log.i(ThreeDS.MY_TAG, "City: "+submittedCity);
            Log.i(ThreeDS.MY_TAG, "Zip: "+submittedZip);
            Log.i(ThreeDS.MY_TAG, "Card Number: "+cardNumber);
            Log.i(ThreeDS.MY_TAG, "Month: "+month);
            Log.i(ThreeDS.MY_TAG, "Year: "+year);

            Double amount = 150.00;
            String clientId = "c1234";
            //set transaction data
            TransactionData transactionData = new TransactionData(clientId,amount,cardNumber,month,year);

            //set additional transaction data


            threeDS.initialize(this.activity,configParameters, locale, uiCustomization);

            try {
                //create transaction
                transaction = threeDS.createTransaction("",messageVersion);
                transaction.setTransactionData(transactionData);
            }
            catch (InvalidAlgorithmParameterException | NoSuchAlgorithmException e){
                e.printStackTrace();
            }

            threeDS.authenticate(transaction);
        }
    }


}
