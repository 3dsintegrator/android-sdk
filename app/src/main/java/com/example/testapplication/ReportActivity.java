package com.example.testapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.threedsintegrator.sdk.ThreeDS;

public class ReportActivity extends AppCompatActivity {

//    ThreeDS threeDS = new ThreeDS();
    private TextView modeView;
    private TextView protocolVersionView;
    private TextView verboseView;
    private TextView warningsView;
    private TextView authorizationView;
    private TextView authenticationView;
    private TextView errorView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.table_layout);

        Intent intent = getIntent();

        modeView = findViewById(R.id.textViewMode);
        protocolVersionView = findViewById(R.id.textViewProtocolVersion);
        verboseView = findViewById(R.id.textViewVerbose);
        warningsView = findViewById(R.id.textViewWarnings);
        authorizationView = findViewById(R.id.textViewAuthorization);
        authenticationView = findViewById(R.id.textViewAuthentication);
        errorView = findViewById(R.id.textViewError);

        //Mode
        try {
            String value = intent.getExtras().getString("mode");
            modeView.setText(value);
        }
        catch (NullPointerException e){
            Log.i(ThreeDS.MY_TAG,e.getMessage());
            modeView.setText("Null");
        }

        //Message Version
        try {
            String value = intent.getExtras().getString("messageVersion");
            protocolVersionView.setText(value);
        }
        catch (NullPointerException e){
            Log.i(ThreeDS.MY_TAG,e.getMessage());
            protocolVersionView.setText("Null");
        }

        //Verbose
        try {
            String value = intent.getExtras().getString("verbose");
            verboseView.setText(value);
        }
        catch (NullPointerException e){
            Log.i(ThreeDS.MY_TAG,e.getMessage());
            verboseView.setText("Null");
        }

        //Warnings
        try {
            String value = intent.getExtras().getString("warnings");
            warningsView.setText(value);
        }
        catch (NullPointerException e){
            Log.i(ThreeDS.MY_TAG,e.getMessage());
            warningsView.setText("Null");
        }

        //Authorization
        try {
            String value = intent.getExtras().getString("authorization");
            authorizationView.setText(value);
        }
        catch (NullPointerException e){
            Log.i(ThreeDS.MY_TAG,e.getMessage());
            authorizationView.setText("Null");
        }

        //Authentication
        try {
            String value = intent.getExtras().getString("authentication");
            authenticationView.setText(value);
        }
        catch (NullPointerException e){
            Log.i(ThreeDS.MY_TAG,e.getMessage());
            authenticationView.setText("Null");
        }


        //Errors
        try {
            String value = intent.getExtras().getString("errors");
            errorView.setText(value);
        }
        catch (NullPointerException e){
            Log.i(ThreeDS.MY_TAG,e.getMessage());
            errorView.setText("Null");
        }




    }
}
