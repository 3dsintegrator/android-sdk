package com.example.testapplication;

import android.Manifest;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.rule.GrantPermissionRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.threedsintegrator.sdk.AuthorizeResponse;
import com.threedsintegrator.sdk.ConfigParameters;
import com.threedsintegrator.sdk.ThreeDS;
import com.threedsintegrator.sdk.ThreeDSIntegratorApi;
import com.threedsintegrator.sdk.TransactionData;
import com.threedsintegrator.sdk.TransactionUtils;
import com.threedsintegrator.sdk.UiCustomization;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.assertEquals;


@RunWith(AndroidJUnit4.class)
public class ThreeDSChallengeTest {
    private ThreeDS threeDS2Service;
    private Context applicationContext;
    private ConfigParameters configParameters;
    private UiCustomization uiCustomization;
    private String locale;
    private String authorization;
    private String apiKey;
    private TransactionData transactionData;
    public static final String MY_TAG = "my_android_tag";

    @Before
    public void setUp() throws Exception {
        this.applicationContext = InstrumentationRegistry.getTargetContext();
        this.apiKey = "qI83kBQ49N9PmWiNCC8zNpb8Kxhb1FcI";

        this.configParameters = new ConfigParameters();
        this.configParameters.addParam("","apiKey",apiKey);
        this.configParameters.addParam("","mode","sandbox");
        this.configParameters.addParam("","verbose","true");
        this.uiCustomization = new UiCustomization();
        this.locale = "";

        this.threeDS2Service = new ThreeDS();
        this.threeDS2Service.initialize(this.applicationContext, this.configParameters, locale, uiCustomization);
    }

    //https://www.vogella.com/tutorials/AndroidTesting/article.html#activity_testing
    @Rule
    public ActivityTestRule<MainActivity> rule  = new  ActivityTestRule<>(MainActivity.class);

    @Rule
    public GrantPermissionRule mRuntimePermissionRule = GrantPermissionRule.grant(Manifest.permission.INTERNET);

    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.example.testapplication", appContext.getPackageName());
    }

//    @Test
//    public void doChallengeTestTimer() {
//
//    }

//    @Test(expected= InvalidInputException.class)
//    public void doChallengeBadTimeOut() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException {
//        //create all params
//        //SDK to timeout in 5 minutes
//        int timeOut = 4;
//
//        //create test activity
//        MainActivity activity = rule.getActivity();
//        assertNotNull(activity);
//
//        //create challengeParameters
//        ChallengeParameters challengeParameters = new ChallengeParameters();
//        challengeParameters.set3DSServerTransactionID("0010");
//        challengeParameters.setAcsTransactionID("001");
//        challengeParameters.setAcsRefNumber("0010");
//        //on page 33 of EMVCo_3DS_-AppBased_CryptoExamples_082018 (1).pdf- signature take from page 51
//        challengeParameters.setAcsSignedContent("eyJhbGciOiJFUzI1NiIsIng1YyI6WyJNSUlDclRDQ0FaV2dBd0lCQWdJUWJTNEM0QlNpZzd1dUo1dURwZVQ0V1RBTkJna3Foa2lHOXcwQkFRc0ZBREJITVJNd0VRWUtDWkltaVpQeUxHUUJHUllEWTI5dE1SY3dGUVlLQ1pJbWlaUHlMR1FCR1JZSFpYaGhiWEJzWlRFWE1CVUdBMVVFQXd3T1VsTkJJRVY0WVcxd2JHVWdSRk13SGhjTk1UY3hNVEl4TVRVME16STNXaGNOTWpjeE1qTXhNVE16TURBd1dqQkhNUk13RVFZS0NaSW1pWlB5TEdRQkdSWURZMjl0TVJjd0ZRWUtDWkltaVpQeUxHUUJHUllIWlhoaGJYQnNaVEVYTUJVR0ExVUVBd3dPUlVNZ1JYaGhiWEJzWlNCQlExTXdXVEFUQmdjcWhrak9QUUlCQmdncWhrak9QUU1CQndOQ0FBVGZvZml3YzZBaXUxWWc1dkc5ZUtYSGVEQ1ZoOWgzVk1xTjIveUoxQ1dHVWlwOEJqOHErZXJPbzc0dHQ2akVUTXpBYVRwekxaNW9HRFpjZVpmR21NN2RvMkF3WGpBTUJnTlZIUk1CQWY4RUFqQUFNQTRHQTFVZER3RUIvd1FFQXdJSGdEQWRCZ05WSFE0RUZnUVUwQVd0REhSL3ZsUXJSQXo0YUtnSkJsbkZqRXN3SHdZRFZSMGpCQmd3Rm9BVXc0TUNuYndENm0yd3Bub1Eyc05EOEdyeVBONHdEUVlKS29aSWh2Y05BUUVMQlFBRGdnRUJBRXFsRVJld1VDZUV0dEFrQzBGMTZIamp4ZnYxV2E4bmFEbWFSTDk5UTAvcXFVTjh3MHF3cEFQRjd3bjJhZkxmYUdkKzV1WkViMVROWXdWOUF3OUwvczNCY1NURVJJbDZPRVduK3g3Y3RPbUh5MnZ2N21pdGFVcmlsZUdvZGVubS9mYURkeTVWZ0tZaitLc01WTTJzTlZhZWtYK1Qwc3dBQ1g5QjkwdW5aeGE2MjU2dDJPSjJRVjV6dTNzWU8xTjBqOXY3K3lGK0ZneDAxNE5ydzcvWHQ4SUxHRjU4TnhiUWhraGtmV1NmSHRhRTVtb0JBYldSdUZURmJrQmY0NVNLZTBVTWlVNUxhYzl4STBPN1hDRCt6TkI1bXdzNE5PMkFZdnl4SHE5WCthNjRJaFhjbFhuZ1BRTXJVcU1vTFdJMTY2Z1JKU3ZRRVdzSUxJVXR4MndzaVlzPSJdfQ.eyJBQ1MgRXBoZW1lcmFsIFB1YmxpYyBLZXkgKFFUKSI6eyJrdHkiOiJFQyIsImNydiI6IlAtMjU2IiwieCI6Im1QVUtUX2JBV0dISWhnMFRwampxVnNQMXJYV1F1X3Z3Vk9ISHROa2RZb0EiLCJ5IjoiOEJRQXNJbUdlQVM0NmZ5V3c1TWhmR1RUMElqQnBGdzJTUzM0RHY0SXJzIix9LCJTREsgRXBoZW1lcmFsIFB1YmxpYyBLZXkgKFFDKSI6eyJrdHkiOiJFQyIsImNydiI6IlAtMjU2IiwieCI6IlplMmxvU1Yzd3Jyb0tVTl80emh3R2hDcW8zWGh1MXRkNFFqZVE1d0lWUjAiLCJ5IjoiSGxMdGRYQVJZX2Y1NUEzZm56UWJQY202aGdyMzRNcDhwLW51elFDRTBadyIsfSwiQUNTIFVSTCI6Imh0dHA6Ly9hY3NzZXJ2ZXIuZG9tYWlubmFtZS5jb20ifQ");
//        challengeParameters.setThreeDSRequestorAppURL("www.url.com");
//
//        //create test transaction
//        Transaction transaction = this.threeDS2Service.createTransaction("001","001");
//
//        transaction.doChallenge(activity, challengeParameters, new ChallengeStatusReceiver() {
//            //see  pg 42
//            @Override
//            public void completed(CompletionEvent e) {
//                //a transaction status shall be available.
//            }
//
//            @Override
//            public void cancelled() {
//                //card holder cancels transaction
//
//                //1. end the challenge flow
//                //2. send notification to the requester app that event was cancelled- app will then display the appropriate screen
//            }
//
//            @Override
//            public void timedout() {
//                //challenge process reaches or exceeds the timeout
//
//                //1. sdk shall make an a best effort to stop the challenge flow
//                //2. send notification about the timed out event to the 3DS Requestor App- app will then display the appropriate screen
//            }
//
//            @Override
//            public void protocolError(ProtocolErrorEvent e) {
//                //SDK receives an EMV 3-D Secure protocol-defined error message from the ACS (i.e any error returned by the ACS)
//
//                //1. end the challenge flow
//                //2. send error code and details from the ACS error message as a notification to the 3DS Requestor App
//            }
//
//            @Override
//            public void runtimeError(RuntimeErrorEvent e) {
//                //all errors except those covered by the protocolError method
//
//                //1. end challenge flow
//                //2. send notification about the run-time error event to the 3DS Requestor App
//            }
//        }, timeOut);
//    }
//
//    @Test
//    public void doChallenge() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException {
//        //create all params
//        //SDK to timeout in 5 minutes
//        int timeOut = 5;
//
//        //create test activity
//        MainActivity activity = rule.getActivity();
//        assertNotNull(activity);
//
//        //create challengeParameters
//        ChallengeParameters challengeParameters = new ChallengeParameters();
//        challengeParameters.set3DSServerTransactionID("0010");
//        challengeParameters.setAcsTransactionID("001");
//        challengeParameters.setAcsRefNumber("0010");
//        //on page 33 of EMVCo_3DS_-AppBased_CryptoExamples_082018 (1).pdf- signature take from page 51
//        challengeParameters.setAcsSignedContent("eyJhbGciOiJFUzI1NiIsIng1YyI6WyJNSUlDclRDQ0FaV2dBd0lCQWdJUWJTNEM0QlNpZzd1dUo1dURwZVQ0V1RBTkJna3Foa2lHOXcwQkFRc0ZBREJITVJNd0VRWUtDWkltaVpQeUxHUUJHUllEWTI5dE1SY3dGUVlLQ1pJbWlaUHlMR1FCR1JZSFpYaGhiWEJzWlRFWE1CVUdBMVVFQXd3T1VsTkJJRVY0WVcxd2JHVWdSRk13SGhjTk1UY3hNVEl4TVRVME16STNXaGNOTWpjeE1qTXhNVE16TURBd1dqQkhNUk13RVFZS0NaSW1pWlB5TEdRQkdSWURZMjl0TVJjd0ZRWUtDWkltaVpQeUxHUUJHUllIWlhoaGJYQnNaVEVYTUJVR0ExVUVBd3dPUlVNZ1JYaGhiWEJzWlNCQlExTXdXVEFUQmdjcWhrak9QUUlCQmdncWhrak9QUU1CQndOQ0FBVGZvZml3YzZBaXUxWWc1dkc5ZUtYSGVEQ1ZoOWgzVk1xTjIveUoxQ1dHVWlwOEJqOHErZXJPbzc0dHQ2akVUTXpBYVRwekxaNW9HRFpjZVpmR21NN2RvMkF3WGpBTUJnTlZIUk1CQWY4RUFqQUFNQTRHQTFVZER3RUIvd1FFQXdJSGdEQWRCZ05WSFE0RUZnUVUwQVd0REhSL3ZsUXJSQXo0YUtnSkJsbkZqRXN3SHdZRFZSMGpCQmd3Rm9BVXc0TUNuYndENm0yd3Bub1Eyc05EOEdyeVBONHdEUVlKS29aSWh2Y05BUUVMQlFBRGdnRUJBRXFsRVJld1VDZUV0dEFrQzBGMTZIamp4ZnYxV2E4bmFEbWFSTDk5UTAvcXFVTjh3MHF3cEFQRjd3bjJhZkxmYUdkKzV1WkViMVROWXdWOUF3OUwvczNCY1NURVJJbDZPRVduK3g3Y3RPbUh5MnZ2N21pdGFVcmlsZUdvZGVubS9mYURkeTVWZ0tZaitLc01WTTJzTlZhZWtYK1Qwc3dBQ1g5QjkwdW5aeGE2MjU2dDJPSjJRVjV6dTNzWU8xTjBqOXY3K3lGK0ZneDAxNE5ydzcvWHQ4SUxHRjU4TnhiUWhraGtmV1NmSHRhRTVtb0JBYldSdUZURmJrQmY0NVNLZTBVTWlVNUxhYzl4STBPN1hDRCt6TkI1bXdzNE5PMkFZdnl4SHE5WCthNjRJaFhjbFhuZ1BRTXJVcU1vTFdJMTY2Z1JKU3ZRRVdzSUxJVXR4MndzaVlzPSJdfQ.eyJBQ1MgRXBoZW1lcmFsIFB1YmxpYyBLZXkgKFFUKSI6eyJrdHkiOiJFQyIsImNydiI6IlAtMjU2IiwieCI6Im1QVUtUX2JBV0dISWhnMFRwampxVnNQMXJYV1F1X3Z3Vk9ISHROa2RZb0EiLCJ5IjoiOEJRQXNJbUdlQVM0NmZ5V3c1TWhmR1RUMElqQnBGdzJTUzM0RHY0SXJzIix9LCJTREsgRXBoZW1lcmFsIFB1YmxpYyBLZXkgKFFDKSI6eyJrdHkiOiJFQyIsImNydiI6IlAtMjU2IiwieCI6IlplMmxvU1Yzd3Jyb0tVTl80emh3R2hDcW8zWGh1MXRkNFFqZVE1d0lWUjAiLCJ5IjoiSGxMdGRYQVJZX2Y1NUEzZm56UWJQY202aGdyMzRNcDhwLW51elFDRTBadyIsfSwiQUNTIFVSTCI6Imh0dHA6Ly9hY3NzZXJ2ZXIuZG9tYWlubmFtZS5jb20ifQ");
//        challengeParameters.setThreeDSRequestorAppURL("www.url.com");
//
//        //create test transaction
//        Transaction transaction = this.threeDS2Service.createTransaction("001","001");
//
//        transaction.doChallenge(activity, challengeParameters, new ChallengeStatusReceiver() {
//            //see  pg 42
//            @Override
//            public void completed(CompletionEvent e) {
//                //a transaction status shall be available.
//            }
//
//            @Override
//            public void cancelled() {
//                //card holder cancels transaction
//
//                //1. end the challenge flow
//                //2. send notification to the requester app that event was cancelled- app will then display the appropriate screen
//            }
//
//            @Override
//            public void timedout() {
//                //challenge process reaches or exceeds the timeout
//
//                //1. sdk shall make an a best effort to stop the challenge flow
//                //2. send notification about the timed out event to the 3DS Requestor App- app will then display the appropriate screen
//            }
//
//            @Override
//            public void protocolError(ProtocolErrorEvent e) {
//                //SDK receives an EMV 3-D Secure protocol-defined error message from the ACS (i.e any error returned by the ACS)
//
//                //1. end the challenge flow
//                //2. send error code and details from the ACS error message as a notification to the 3DS Requestor App
//            }
//
//            @Override
//            public void runtimeError(RuntimeErrorEvent e) {
//                //all errors except those covered by the protocolError method
//
//                //1. end challenge flow
//                //2. send notification about the run-time error event to the 3DS Requestor App
//            }
//        }, timeOut);
//
//        //**continue from page 49**
//
//    }

    //https://stackoverflow.com/questions/40932103/how-to-unit-test-http-request-android
    @Test
    public void sendCReq() throws JSONException {
        JSONObject CReq = new JSONObject();
        CReq.put("threeDSServerTransID","8a880dc0-d2d2-4067-bcb1-b08d1690b26e");
        CReq.put("acsTransID","d7c1ee99-9478-44a6-b1f2-391e29c6b340");
        CReq.put("messageType","CReq");
        CReq.put("messageVersion","2.1.0");
        CReq.put("sdkTransID","b2385523-a66c-4907-ac3c-91848e8c0067");
        CReq.put("sdkCounterStoA","001");

        String expectedCReq = "{\"threeDSServerTransID\":\"8a880dc0-d2d2-4067-bcb1-b08d1690b26e\",\"acsTransID\":\"d7c1ee99-9478-44a6-b1f2-391e29c6b340\",\"messageType\":\"CReq\",\"messageVersion\":\"2.1.0\",\"sdkTransID\":\"b2385523-a66c-4907-ac3c-91848e8c0067\",\"sdkCounterStoA\":\"001\"}";
        assertEquals(expectedCReq,CReq.toString());
        TransactionUtils.sendCReq("",CReq.toString());
    }

    @Test
    public void receiveCReq() throws JSONException {
        String expectedCRes = "{\"threeDSServerTransID\":\"8a880dc0-d2d2-4067-bcb1-b08d1690b26e\",\"acsTransID\":\"d7c1ee99-9478-44a6-b1f2-391e29c6b340\",\"acsUiType\":\"01\",\"challengeAddInfo\":\"Additional information to be shown.\",\"challengeCompletionInd\":\"N\",\"challengeInfoHeader\":\"Header information\",\"challengeInfoLabel\":\"One-time-password\",\"challengeInfoText\":\"Please enter the received one-time-password\",\"challengeInfoTextIndicator\":\"N\",\"expandInfoLabel\":\"Additional instructions\",\"expandInfoText\":\"The issuer will send you via SMS a one-time password.Please enter the value in the designated input field above and press continue to complete the 3-D Secure authentication process.\",\"issuerImage\":{\"medium\":\"https://acs.com/medium_image.svg\",\"high\":\"https://acs.com/high_image.svg\",\"extraHigh\":\"https://acs.com/extraHigh_image.svg\"},\"messageType\":\"CRes\",\"messageVersion\":\"2.1.0\",\"psImage\":{\"medium\":\"https://ds.com/medium_image.svg\",\"high\":\"https://ds.com/high_image.svg\",\"extraHigh\":\"https://ds.com/extraHigh_image.svg\"},\"resendInformationLabel\":\"Send new One-time-password\",\"sdkTransID\":\"b2385523-a66c-4907-ac3c-91848e8c0067\",\"submitAuthenticationLabel\":\"Continue\",\"whyInfoLabel\":\"Why using 3-D Secure?\",\"whyInfoText\":\"Some explanation about why using 3-D Secure is an excellent idea as part of an online payment transaction\",\"acsCounterAtoS\":\"001\"}";
    }

}
