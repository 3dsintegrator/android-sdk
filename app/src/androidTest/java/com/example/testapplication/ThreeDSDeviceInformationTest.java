package com.example.testapplication;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.rule.GrantPermissionRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.threedsintegrator.sdk.ConfigParameters;
import com.threedsintegrator.sdk.DeviceInformation;
import com.threedsintegrator.sdk.ThreeDS;
import com.threedsintegrator.sdk.TransactionData;
import com.threedsintegrator.sdk.UiCustomization;
import com.threedsintegrator.sdk.Warning;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertNotEquals;


@RunWith(AndroidJUnit4.class)
public class ThreeDSDeviceInformationTest {
    private ThreeDS threeDS2Service;
    private Context applicationContext;
    private ConfigParameters configParameters;
    private UiCustomization uiCustomization;
    private String locale;
    private String authorization;
    private String apiKey;
    private TransactionData transactionData;
    public static final String MY_TAG = "my_android_tag";

    @Before
    public void setUp() throws Exception {
        this.applicationContext = InstrumentationRegistry.getTargetContext();
    }

    @Rule
    public ActivityTestRule<MainActivity> rule  = new  ActivityTestRule<>(MainActivity.class);

    @Rule
    public GrantPermissionRule mRuntimePermissionRule = GrantPermissionRule.grant(Manifest.permission.INTERNET,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.READ_PHONE_STATE,Manifest.permission.ACCESS_WIFI_STATE);


    @Test
    public void getDeviceInformation(){
        //Setup Warnings
        Warning warningJailBroken = new Warning("SW01","The device is jailbroken.", Warning.Severity.HIGH);
        Warning warningEmulator = new Warning("SW03","An emulator is being used to run the App.", Warning.Severity.HIGH);

        ArrayList<Warning> warnings = new ArrayList<Warning>();
        warnings.add(warningJailBroken);
        warnings.add(warningEmulator);

        DeviceInformation deviceInformation = new DeviceInformation(this.applicationContext,warnings);
//        Log.i(ThreeDS.MY_TAG,"Device Information: "+deviceInformation.convertToJson());

        //Needed to output large logcat
        if (deviceInformation.convertToJson().length() > 4000) {
            Log.v(ThreeDS.MY_TAG, "sb.length = " + deviceInformation.convertToJson().length());
            int chunkCount = deviceInformation.convertToJson().length() / 4000;     // integer division
            for (int i = 0; i <= chunkCount; i++) {
                int max = 4000 * (i + 1);
                if (max >= deviceInformation.convertToJson().length()) {
                    Log.v(ThreeDS.MY_TAG, "chunk " + i + " of " + chunkCount + ":" + deviceInformation.convertToJson().substring(4000 * i));
                } else {
                    Log.v(ThreeDS.MY_TAG, "chunk " + i + " of " + chunkCount + ":" + deviceInformation.convertToJson().substring(4000 * i, max));
                }
            }
        } else {
            Log.v(ThreeDS.MY_TAG, deviceInformation.convertToJson().toString());
        }
    }

    @Test
    public void getLatitude(){
        LocationManager locationManager = (LocationManager)this.applicationContext.getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        try {
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null){
                double latitude = location.getLatitude();
                Log.i(ThreeDS.MY_TAG,"LAT: "+Double.toString(latitude));
            }
            else {
                Log.i(ThreeDS.MY_TAG,"Location is null");
            }

        }
        catch (SecurityException | NullPointerException e){
            Log.i(ThreeDS.MY_TAG,"Error: "+e.getMessage());
        }
    }

    @Test
    public void getLongitude(){
        LocationManager locationManager = (LocationManager)this.applicationContext.getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        try {
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null){
                double longitude = location.getLongitude();
                Log.i(ThreeDS.MY_TAG,"LON: "+Double.toString(longitude));
            }
            else {
                Log.i(ThreeDS.MY_TAG,"Location is null");
            }
        }
        catch (SecurityException | NullPointerException e){
            Log.i(ThreeDS.MY_TAG,"Error: "+e.getMessage());
        }
    }




}
