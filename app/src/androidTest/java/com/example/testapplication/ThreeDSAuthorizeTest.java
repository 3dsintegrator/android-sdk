package com.example.testapplication;

import android.Manifest;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.rule.GrantPermissionRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.threedsintegrator.sdk.AuthenticateResponse;
import com.threedsintegrator.sdk.AuthorizeResponse;
import com.threedsintegrator.sdk.ConfigParameters;
import com.threedsintegrator.sdk.PublicKeyClass;
import com.threedsintegrator.sdk.SdkRequired;
import com.threedsintegrator.sdk.ThreeDS;
import com.threedsintegrator.sdk.ThreeDSIntegratorApi;
import com.threedsintegrator.sdk.Transaction;
import com.threedsintegrator.sdk.TransactionData;
import com.threedsintegrator.sdk.UiCustomization;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;

import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.assertEquals;


@RunWith(AndroidJUnit4.class)
public class ThreeDSAuthorizeTest {
    private ThreeDS threeDS2Service;
    private Context applicationContext;
    private ConfigParameters configParameters;
    private UiCustomization uiCustomization;
    private String locale;
    private String authorization;
    private String apiKey;
    private TransactionData transactionData;
    public static final String MY_TAG = "my_android_tag";

    @Before
    public void setUp() throws Exception {
        this.applicationContext = InstrumentationRegistry.getTargetContext();
        this.apiKey = "qI83kBQ49N9PmWiNCC8zNpb8Kxhb1FcI";

        this.configParameters = new ConfigParameters();
        this.configParameters.addParam("","apiKey",apiKey);
        this.configParameters.addParam("","mode","sandbox");
        this.configParameters.addParam("","verbose","true");
        this.uiCustomization = new UiCustomization();
        this.locale = "";

        this.threeDS2Service = new ThreeDS();
        this.threeDS2Service.initialize(this.applicationContext, this.configParameters, locale, uiCustomization);
    }

    @Rule
    public ActivityTestRule<MainActivity> rule  = new  ActivityTestRule<>(MainActivity.class);

    @Rule
    public GrantPermissionRule mRuntimePermissionRule = GrantPermissionRule.grant(Manifest.permission.INTERNET);

    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("com.example.testapplication", appContext.getPackageName());
    }

    @Test
    public void ThreeDSAuthorizeSuccess() {
        Log.i(MY_TAG, "Authorize called");
        String bearer = "";

        //For sandbox live
        String sandboxUrl = "https://api-sandbox.3dsintegrator.com/v2/";
        String apiKey = "qI83kBQ49N9PmWiNCC8zNpb8Kxhb1FcI";

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(sandboxUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ThreeDSIntegratorApi threeDSIntegratorApi = retrofit.create(ThreeDSIntegratorApi.class);

        Call<AuthorizeResponse> call = threeDSIntegratorApi.authorize(apiKey);

        try {
            Response<AuthorizeResponse> response = call.execute();
            Log.i(MY_TAG, "Error Code: "+response.code());
            if (!response.isSuccessful()){

                Log.i(MY_TAG, "Authorize response not successful");
                Log.i(MY_TAG, "Error Code: "+response.code());
                try {
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    Log.i(MY_TAG,"Authorize Error Message: "+jObjError.getString("error"));
                }
                catch (JSONException e){
                    Log.i(MY_TAG, "JSON Exception: "+e.getMessage());
                }
            }else {
                Log.i(MY_TAG, "Authorize response successful");
                Log.i(MY_TAG, "Error Code: "+response.code());
                AuthorizeResponse authorizeResponse = response.body();
                Log.i(MY_TAG, "Authorize Message: "+authorizeResponse.getMessage());

                Headers responseHeaders = response.headers();
                bearer = responseHeaders.get("authorization");
                Log.i(MY_TAG, "Authorization: "+bearer);
            }

            this.authorization = bearer;
            assertEquals(200,response.code());
        }
        catch (IOException e){
            Log.i(MY_TAG, "Authorize Exception: "+e.getMessage());
        }

        Log.i(MY_TAG, "Authorize done");
    }

    @Test
    public void ThreeDSAuthorizeError() {
        Log.i(MY_TAG, "Authorize called");
        String bearer = "";

        //For sandbox live
        String sandboxUrl = "https://api-sandbox.3dsintegrator.com/v2/";
        String apiKey = "qI83kBQ49N9PmWiNCC8zNpb8Kxhb123";

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(sandboxUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ThreeDSIntegratorApi threeDSIntegratorApi = retrofit.create(ThreeDSIntegratorApi.class);

        Call<AuthorizeResponse> call = threeDSIntegratorApi.authorize(apiKey);

        try {
            Response<AuthorizeResponse> response = call.execute();
            Log.i(MY_TAG, "Error Code: "+response.code());
            if (!response.isSuccessful()){
                Log.i(MY_TAG, "Authorize response not successful");
                Log.i(MY_TAG, "Error Code: "+response.code());
                try {
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    Log.i(MY_TAG,"Authorize Error Message: "+jObjError.getString("error"));
                }
                catch (JSONException e){
                    Log.i(MY_TAG, "JSON Exception: "+e.getMessage());
                }
            }else {
                Log.i(MY_TAG, "Authorize response successful");
                Log.i(MY_TAG, "Error Code: "+response.code());
                AuthorizeResponse authorizeResponse = response.body();
                Log.i(MY_TAG, "Authorize Message: "+authorizeResponse.getMessage());

                Headers responseHeaders = response.headers();
                bearer = responseHeaders.get("authorization");
                Log.i(MY_TAG, "Authorization: "+bearer);
            }

            this.authorization = bearer;
            assertEquals(401,response.code());
        }
        catch (IOException e){
            Log.i(MY_TAG, "Authorize Exception: "+e.getMessage());
        }

        Log.i(MY_TAG, "Authorize done");
    }

}
