package com.example.testapplication;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.rule.GrantPermissionRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.threedsintegrator.sdk.AuthorizeResponse;
import com.threedsintegrator.sdk.ConfigParameters;
import com.threedsintegrator.sdk.ThreeDS;
import com.threedsintegrator.sdk.ThreeDSIntegratorApi;
import com.threedsintegrator.sdk.TransactionData;
import com.threedsintegrator.sdk.UiCustomization;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.UUID;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;


@RunWith(AndroidJUnit4.class)
public class ThreeDSGenerateAppIdTest {
    private ThreeDS threeDS2Service;
    private Context applicationContext;
    private ConfigParameters configParameters;
    private UiCustomization uiCustomization;
    private String locale;
    private String authorization;
    private String apiKey;
    private TransactionData transactionData;
    public static final String MY_TAG = "my_android_tag";

    @Before
    public void setUp() throws Exception {
        this.applicationContext = InstrumentationRegistry.getTargetContext();
    }

    @Rule
    public ActivityTestRule<MainActivity> rule  = new  ActivityTestRule<>(MainActivity.class);

    @Rule
    public GrantPermissionRule mRuntimePermissionRule = GrantPermissionRule.grant(Manifest.permission.INTERNET);


    @Test
    public void generateAppIDTest() {
        SharedPreferences sharedPref = this.applicationContext.getSharedPreferences("SDK_APP_ID",Context.MODE_PRIVATE);
        String appID = sharedPref.getString("appID","No App ID");
        if (appID.equals("No App ID")){
            String newAppID = UUID.randomUUID().toString();
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("appID", newAppID);
            editor.commit();
            Log.i(ThreeDS.MY_TAG,"New ID: "+newAppID);
            assertNotEquals("No App ID",newAppID);
        }

        Log.i(ThreeDS.MY_TAG,"ID: "+appID);
    }

    @Test
    public void getAppIDTest() {
        SharedPreferences sharedPref = this.applicationContext.getSharedPreferences("SDK_APP_ID",Context.MODE_PRIVATE);
        String appID = sharedPref.getString("appID","No App ID");
        Log.i(ThreeDS.MY_TAG,"ID: "+appID);
    }




}
