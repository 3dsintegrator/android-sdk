package com.example.testapplication;

import android.Manifest;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.rule.GrantPermissionRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.threedsintegrator.sdk.AuthenticateResponse;
import com.threedsintegrator.sdk.AuthenticationRequestParameters;
import com.threedsintegrator.sdk.AuthorizeResponse;
import com.threedsintegrator.sdk.ConfigParameters;
import com.threedsintegrator.sdk.PublicKeyClass;
import com.threedsintegrator.sdk.SdkRequired;
import com.threedsintegrator.sdk.ThreeDS;
import com.threedsintegrator.sdk.ThreeDSIntegratorApi;
import com.threedsintegrator.sdk.Transaction;
import com.threedsintegrator.sdk.TransactionData;
import com.threedsintegrator.sdk.TransactionUtils;
import com.threedsintegrator.sdk.UiCustomization;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECPoint;

import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;


@RunWith(AndroidJUnit4.class)
public class ThreeDSAuthenticateTest {
    private ThreeDS threeDS2Service;
    private Context applicationContext;
    private ConfigParameters configParameters;
    private UiCustomization uiCustomization;
    private String locale;
    private String authorization;
    private TransactionData transactionData;
    public static final String MY_TAG = "my_android_tag";

    @Before
    public void setUp() throws Exception {
        this.applicationContext = InstrumentationRegistry.getTargetContext();
        this.uiCustomization = new UiCustomization();
        this.locale = "";
    }

    //https://www.vogella.com/tutorials/AndroidTesting/article.html#activity_testing
    @Rule
    public ActivityTestRule<MainActivity> rule  = new  ActivityTestRule<>(MainActivity.class);

    @Rule
    public GrantPermissionRule mRuntimePermissionRule = GrantPermissionRule.grant(Manifest.permission.INTERNET);

    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("com.example.testapplication", appContext.getPackageName());
    }

    @Test
    public void ThreeDSAuthenticateWithActualData() {
        Log.i(MY_TAG, "Authenticate called");
        Transaction transaction;

        String sandboxUrl = "https://api-sandbox.3dsintegrator.com/v2/";
        String apiKey = "qI83kBQ49N9PmWiNCC8zNpb8Kxhb1FcI";

        this.configParameters = new ConfigParameters();
        this.configParameters.addParam("","apiKey",apiKey);
        this.configParameters.addParam("","mode","sandbox");
        this.configParameters.addParam("","verbose","true");

        ThreeDS threeDS = new ThreeDS();
        threeDS.initialize(this.applicationContext,this.configParameters,this.locale,this.uiCustomization);

        try {
            //create transaction
            transaction = threeDS.createTransaction("","2.1.0");

            threeDS.authorize();
            String bearer = threeDS.getAuthorization();

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.level(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            Log.i(MY_TAG, "setup retrofit");
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(sandboxUrl)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ThreeDSIntegratorApi threeDSIntegratorApi = retrofit.create(ThreeDSIntegratorApi.class);
            //set the public key

            //------
//            TransactionUtils transactionUtils = new TransactionUtils();
//            transactionUtils.setReceiverPublicKey(transaction.getTransactionKeyPair().getPublic());
            //----

            ECPublicKey publicKey =  (ECPublicKey) transaction.getTransactionKeyPair().getPublic();
            ECPoint ecp = publicKey.getW();
            // to access X and Y you can use
            Log.i(MY_TAG, "access X and Y you can use");
            BigInteger x = ecp.getAffineX();
            BigInteger y = ecp.getAffineY();


            TransactionData transactionData = new TransactionData("c9fc0df7-27ce-4370-b5e5-b222ab0a19f9",150.25,"4005519200000004","08","22");

            //setup public key class
            Log.i(MY_TAG, "setup public key class");
            PublicKeyClass sdkEphemPubKey = new PublicKeyClass("EC","P-256",new String(x.toByteArray()),new String(y.toByteArray()));
            Log.i(MY_TAG, "get authenticationRequestParameters");
            AuthenticationRequestParameters authenticationRequestParameters = transaction.getAuthenticationRequestParameters();
            Log.i(MY_TAG, "setup sdkRequired");
            SdkRequired sdkRequired = new SdkRequired(transaction.getSdkAppID(),sdkEphemPubKey,transaction.getSdkReferenceNumber(),"5",authenticationRequestParameters.getEncryptedDeviceData(),transaction.getSdkTransactionID());
            Log.i(MY_TAG, "set sdkRequired");
            transactionData.setSdk(sdkRequired);

            Log.i(MY_TAG, "Transaction Json Data: "+ transactionData.convertToJson());
            Call<AuthenticateResponse> call = threeDSIntegratorApi.authenticate(apiKey,bearer,transactionData);

            Response<AuthenticateResponse> response = call.execute();
            if (!response.isSuccessful()){
                Log.i(MY_TAG, "Authenticate response not successful");
                Log.i(MY_TAG, "Error Code: "+response.code());
                try {
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    Log.i(MY_TAG,"Authenticate Error Message: "+jObjError.getString("error"));
                    Log.i(MY_TAG,"Authenticate Error Message: "+jObjError.getString("transactionId"));
                    Log.i(MY_TAG,"Authenticate Error Message: "+jObjError.getString("clientTransactionId"));
                    Log.i(MY_TAG,"Authenticate Error Message: "+jObjError.getString("correlationId"));
                }
                catch (JSONException e){
                    Log.i(MY_TAG, "JSON Exception: "+e.getMessage());
                }
                fail();
            }else {
                Log.i(MY_TAG, "Authenticate response successful");
                AuthenticateResponse authenticateResponse = response.body();
                Headers responseHeaders = response.headers();


                Log.i(MY_TAG, "Code: "+response.code());
                Log.i(MY_TAG, "Authenticate Headers: \n"+responseHeaders.toString());
                Log.i(MY_TAG, "ARes Status: "+authenticateResponse.getAuthenticationValue());
                Log.i(MY_TAG, "ARes ECI: "+authenticateResponse.getEci());
                Log.i(MY_TAG, "ARes Status: "+authenticateResponse.getStatus());
                Log.i(MY_TAG, "ARes Protocol Version: "+authenticateResponse.getProtocolVersion());
                Log.i(MY_TAG, "ARes dsTransId: "+authenticateResponse.getDsTransId());
                Log.i(MY_TAG, "ARes acsTransId: "+authenticateResponse.getAcsTransId());
                Log.i(MY_TAG, "ARes cardToken: "+authenticateResponse.getCardToken());
            }

            assertEquals(201,response.code());
        }
        catch (IOException | NullPointerException | InvalidAlgorithmParameterException | NoSuchAlgorithmException e){
            Log.i(MY_TAG, "Authenticate Exception: "+e.getMessage());
            e.printStackTrace();
            fail();
        }

    }

    @Test
    public void ThreeDSAuthenticateSuccess() {
        Log.i(MY_TAG, "Authenticate called");
        Transaction transaction;

        String sandboxUrl = "https://api-sandbox.3dsintegrator.com/v2/";
        String apiKey = "qI83kBQ49N9PmWiNCC8zNpb8Kxhb1FcI";

        this.configParameters = new ConfigParameters();
        this.configParameters.addParam("","apiKey",apiKey);
        this.configParameters.addParam("","mode","sandbox");
        this.configParameters.addParam("","verbose","true");

        ThreeDS threeDS = new ThreeDS();
        threeDS.initialize(this.applicationContext,this.configParameters,this.locale,this.uiCustomization);

        //create transaction
        try {
            transaction = threeDS.createTransaction("","2.1.0");
        }
        catch (InvalidAlgorithmParameterException e){
            Log.i(MY_TAG,e.getMessage());
        }
        catch (NoSuchAlgorithmException e){
            Log.i(MY_TAG,e.getMessage());
        }

        threeDS.authorize();
        String bearer = threeDS.getAuthorization();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(sandboxUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ThreeDSIntegratorApi threeDSIntegratorApi = retrofit.create(ThreeDSIntegratorApi.class);

        TransactionData transactionData = new TransactionData("c9fc0df7-27ce-4370-b5e5-b222ab0a19f9",150.25,"4005519200000004","08","22");
        PublicKeyClass sdkEphemPubKey = new PublicKeyClass("EC","P-256","asdf","asdf");
        SdkRequired sdkRequired = new SdkRequired("123",sdkEphemPubKey,"123","5","123","12345");
        transactionData.setSdk(sdkRequired);

        Log.i(MY_TAG, "Transaction Json Data: "+ transactionData.convertToJson());
        Call<AuthenticateResponse> call = threeDSIntegratorApi.authenticate(apiKey,bearer,transactionData);

        try {
            Response<AuthenticateResponse> response = call.execute();
            if (!response.isSuccessful()){
                Log.i(MY_TAG, "Authenticate response not successful");
                Log.i(MY_TAG, "Error Code: "+response.code());
                try {
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    Log.i(MY_TAG,"Authenticate Error Message: "+jObjError.getString("error"));
                    Log.i(MY_TAG,"Authenticate Error Message: "+jObjError.getString("transactionId"));
                    Log.i(MY_TAG,"Authenticate Error Message: "+jObjError.getString("clientTransactionId"));
                    Log.i(MY_TAG,"Authenticate Error Message: "+jObjError.getString("correlationId"));
                }
                catch (JSONException e){
                    Log.i(MY_TAG, "JSON Exception: "+e.getMessage());
                }
                fail();
            }else {
                Log.i(MY_TAG, "Authenticate response successful");
                AuthenticateResponse authenticateResponse = response.body();
                Headers responseHeaders = response.headers();


                Log.i(MY_TAG, "Code: "+response.code());
                Log.i(MY_TAG, "Authenticate Headers: \n"+responseHeaders.toString());
                Log.i(MY_TAG, "ARes Status: "+authenticateResponse.getAuthenticationValue());
                Log.i(MY_TAG, "ARes ECI: "+authenticateResponse.getEci());
                Log.i(MY_TAG, "ARes Status: "+authenticateResponse.getStatus());
                Log.i(MY_TAG, "ARes Protocol Version: "+authenticateResponse.getProtocolVersion());
                Log.i(MY_TAG, "ARes dsTransId: "+authenticateResponse.getDsTransId());
                Log.i(MY_TAG, "ARes acsTransId: "+authenticateResponse.getAcsTransId());
                Log.i(MY_TAG, "ARes cardToken: "+authenticateResponse.getCardToken());
            }

            assertEquals(201,response.code());
        }
        catch (IOException | NullPointerException e){
            Log.i(MY_TAG, "Authenticate Exception: "+e.getMessage());
            fail();
        }

    }

    @Test
    public void ThreeDSAuthenticateFail() {
        Log.i(MY_TAG, "Authenticate called");
        Transaction transaction;

        String sandboxUrl = "https://api-sandbox.3dsintegrator.com/v2/";
        String apiKey = "qI83kBQ49N9PmWiNCC8zNpb8Kxhb1123";

        this.configParameters = new ConfigParameters();
        this.configParameters.addParam("","apiKey",apiKey);
        this.configParameters.addParam("","mode","sandbox");
        this.configParameters.addParam("","verbose","true");

        ThreeDS threeDS = new ThreeDS();
        threeDS.initialize(this.applicationContext,this.configParameters,this.locale,this.uiCustomization);

        //create transaction
        try {
            transaction = threeDS.createTransaction("","2.1.0");
        }
        catch (InvalidAlgorithmParameterException e){
            Log.i(MY_TAG,e.getMessage());
        }
        catch (NoSuchAlgorithmException e){
            Log.i(MY_TAG,e.getMessage());
        }

        threeDS.authorize();
        String bearer = threeDS.getAuthorization();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(sandboxUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ThreeDSIntegratorApi threeDSIntegratorApi = retrofit.create(ThreeDSIntegratorApi.class);

        TransactionData transactionData = new TransactionData("c9fc0df7-27ce-4370-b5e5-b222ab0a19f9",150.25,"4005519200000004","08","22");
        PublicKeyClass sdkEphemPubKey = new PublicKeyClass("EC","P-256","asdf","asdf");
        SdkRequired sdkRequired = new SdkRequired("123",sdkEphemPubKey,"123","5","123","12345");
        transactionData.setSdk(sdkRequired);

        Log.i(MY_TAG, "Transaction Json Data: "+ transactionData.convertToJson());
        Call<AuthenticateResponse> call = threeDSIntegratorApi.authenticate(apiKey,bearer,transactionData);

        try {
            Response<AuthenticateResponse> response = call.execute();
            if (!response.isSuccessful()){
                Log.i(MY_TAG, "Authenticate response not successful");
                Log.i(MY_TAG, "Error Code: "+response.code());
                try {
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    Log.i(MY_TAG,"Authenticate Error Message: "+jObjError.getString("error"));
                    Log.i(MY_TAG,"Authenticate Error Message: "+jObjError.getString("transactionId"));
                    Log.i(MY_TAG,"Authenticate Error Message: "+jObjError.getString("clientTransactionId"));
                    Log.i(MY_TAG,"Authenticate Error Message: "+jObjError.getString("correlationId"));
                }
                catch (JSONException e){
                    Log.i(MY_TAG, "JSON Exception: "+e.getMessage());
                }
                assertEquals(400,response.code());
            }else {
                fail();
            }
        }
        catch (IOException | NullPointerException e){
            Log.i(MY_TAG, "Authenticate Exception: "+e.getMessage());
            fail();
        }

    }

}
