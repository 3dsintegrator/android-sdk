package com.example.testapplication;

import android.Manifest;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.rule.GrantPermissionRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.threedsintegrator.sdk.AuthenticateResponse;
import com.threedsintegrator.sdk.AuthorizeResponse;
import com.threedsintegrator.sdk.ChallengeParameters;
import com.threedsintegrator.sdk.ChallengeStatusReceiver;
import com.threedsintegrator.sdk.CompletionEvent;
import com.threedsintegrator.sdk.ConfigParameters;
import com.threedsintegrator.sdk.InvalidInputException;
import com.threedsintegrator.sdk.ProtocolErrorEvent;
import com.threedsintegrator.sdk.RuntimeErrorEvent;
import com.threedsintegrator.sdk.ThreeDS;
import com.threedsintegrator.sdk.ThreeDSIntegratorApi;
import com.threedsintegrator.sdk.Transaction;
import com.threedsintegrator.sdk.TransactionData;
import com.threedsintegrator.sdk.TransactionUtils;
import com.threedsintegrator.sdk.UiCustomization;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;

import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;


@RunWith(AndroidJUnit4.class)
public class MainActivityTest {
    private ThreeDS threeDS2Service;
    private Context applicationContext;
    private ConfigParameters configParameters;
    private UiCustomization uiCustomization;
    private String locale;
    private String authorization;
    private String apiKey;
    private TransactionData transactionData;
    public static final String MY_TAG = "my_android_tag";

    @Before
    public void setUp() throws Exception {

    }

    @Rule
    public ActivityTestRule<MainActivity> rule  = new  ActivityTestRule<>(MainActivity.class);

    @Rule
    public GrantPermissionRule mRuntimePermissionRule = GrantPermissionRule.grant(Manifest.permission.INTERNET);

    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("com.example.testapplication", appContext.getPackageName());
    }

}
