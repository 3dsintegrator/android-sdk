3DS Integrator Android SDK
=================================

This is android SDK version of the EMVCo Specification v2.20

[ Installing SDK ](#install)

[ Demo Application ](#demo)

[ Initialize ](#initialize)

* [ SDK Warnings ](#warnings)

[ Authentication ](#authentication)

[ Challenge ](#challenge)

[ Errors ](#errors)

<a name="install"></a>
## Installing SDK
There are three options for installing the Android SDK

#### Option 1 - Dependencies (Recommended):
1. Open your android project. Make sure you are using the "Project" View. Your file system should look as follows if you are using the correct view:
```java
AndroidProjectName
    |_ gradle
    |_ .idea
    |_ app
    |_ gradle
    |_ build.gradle
    |_gardle.properties
    
    ..etc
```

2. In your app/build.gradle add the following (use the latest tag):
```java
implementation 'org.bitbucket.3dsintegrator:android-sdk:1.0.0'
```

3. In your project build.gradle add the following 
```java
maven {
            url 'https://jitpack.io'
        }
```
inside the allprojects parentheses. It should now look something like:
```java
allprojects {
    repositories {
        google()
        jcenter()
        mavenCentral()
        maven {
            url 'https://jitpack.io'
        }
        
    }
}
```  
4. Finally, sync your project (you should be automatically promoted to do so).

#### Option 2 - Git:
1. Clone SDK repository anywhere on your local machine (preferably on the Desktop to make it easy to find).

2. Open your android project. Make sure you are using the "Project" View. Your file system should look as follows if you are using the correct view:
```java
AndroidProjectName
    |_ gradle
    |_ .idea
    |_ app
    |_ gradle
    |_ build.gradle
    |_gardle.properties
    
    ..etc
```

3. In your Gradle Properties (gradle.properties) add the following lines:
```java
SDK_VERSION="1.0.0.0"
SDK_REF_NUMBER="0313013"
```

4. In the header section click on: File -> New -> Import Module and find the path of SDK that was pulled via git.
Select "sdk" and open (example path: "Desktop/android-sdk/sdk"). This will import the sdk as a module.

5. Inside the app build.gradle ("app/build.gradle") of your android project add the following line under "dependencies" :

```java
implementation project(':sdk') 

//Required dependencies:
    
//to check if device is rooted
implementation 'com.scottyab:rootbeer-lib:0.0.7' 

//Handle Http request and logging of those requests
implementation 'com.squareup.retrofit2:retrofit:2.6.2'
implementation 'com.squareup.retrofit2:converter-gson:2.6.2'
implementation 'com.squareup.okhttp3:logging-interceptor:4.2.1'
```

6. Update the build.gradle (You will automatically be prompted to "Sync now" when you make changes to the build.gradle file )

#### Option 3 - File Import:
1. Open your Android Project and make sure you using the "Project" View. Your file system should look as follows if you are using the correct view:
```java
AndroidProjectName
    |_ gradle
    |_ .idea
    |_ app
    |_ gradle
    |_ build.gradle
    |_gardle.properties
    
    ..etc
```
2. Right click on the app folder and select "Open Module Settings" from the menu.

3. In the window that pops up select "Modules" from the left hand column, and select "app". 

4. Click the "plus" sign to add the module

5. Select "Import .JAR/ .AAR Package" from the options and click next

6. Locate the .aar file provided and give it a sub project name of "sdk" then click "Apply"

7. Go to dependencies in the left column

8. Click on app, then "libs" from the dependency list

9. Click on the plus sign "Add Dependency" at the top of THAT list

10. From the submenu that pops up select "Module Dependency"

11. Select the sdk and from dropdown ensure "implementation" is selected then click "OK"

12. Finally, add the following to your app's build.gradle file

```java
dependencies {
    //Required dependencies:
    
    //to check if device is rooted
    implementation 'com.scottyab:rootbeer-lib:0.0.7' 
    
    //Handle Http request and logging of those requests
    implementation 'com.squareup.retrofit2:retrofit:2.6.2'
    implementation 'com.squareup.retrofit2:converter-gson:2.6.2'
    implementation 'com.squareup.okhttp3:logging-interceptor:4.2.1'
}
```


<a name="demo"></a>
## Demo Application
In addition to the SDK module, there is also a Demo Application in this project and it makes use of the SDK. To RUN the application:

1. Clone the repository.

2. Once you've open the project in Android Studio it should automatically build and add local.properties file to your project

3. Run the app and follow the instructions in the app. 

  

<a name="initialize"></a>
## Initialize ()
This method is called at the start of the payment stage of the transaction. The app passes configuration parameters, UI configuration parameters, and (optionally) user locale to this method.

Below is a snippet of code that shows the function signature:

```java
public void initialize(android.content.Context applicationContext, ConfigParameters configParameters, String locale, UiCustomization uiCustomization) throws InvalidInputException, SDKAlreadyInitializedException, SDKRuntimeException
```

#### Parameters:
###### a. ApplicationContext
An instance of Android application context

###### b. ConfigParameters 
The ConfigParameters class shall represent the configuration parameters that are required by the 3DS SDK for initialization.


Functions:
- **addParam(String group, String paramName)** -  method shall add a configuration parameter either to the specified group or to the default group, if the group is not specified.
    ```java
    String value = configParameters.addParam(ConfigParameters.DEFAULT_GROUP,"Color");
    ```

- **getParam(String group, String paramName)** -  retrieves a previously added parameter
    ```java
    String value = configParameters.getParamValue(ConfigParameters.DEFAULT_GROUP,"Color");
    ```

- **removeParam(String group, String paramName)** -  removes a parameter that was previously added. Example: 
    ```java
    String value = configParameters.removeParam(ConfigParameters.DEFAULT_GROUP,"Color");
    ```


The following can be initialized via this class by using **addParam(String group, String paramName, String paramValue)**:

1. Verbose
2. Mode (sandbox/production)
3. API Key

See code snippet below:

```java
ConfigParameters configParameters = new ConfigParameters();
configParameters.addParam(ConfigParameters.DEFAULT_GROUP,"verbose","true");
configParameters.addParam(ConfigParameters.DEFAULT_GROUP,"mode","sandbox");
configParameters.addParam(ConfigParameters.DEFAULT_GROUP,"apiKey","your_api_key");
```

A group does not need to be specified. Therefore it can be left as an empty string or use the default group as seen above.

###### c. Locale
String that represents the locale for the app�s user interface. Example:
```java
String locale = "en-US";
```

###### d. UiCustomization
UI configuration information that is used to specify the UI layout and theme. For example, font style and font size.
An object of this class holds various UI-related parameters. The UICustomization object and the objects 
held by it, such as ButtonCustomization, ToolbarCustomization, LabelCustomization, and TextBoxCustomization, 
shall be immutable.


1. Class Customization
The Customization class shall serve as a superclass for the ButtonCustomization class, ToolbarCustomization class, LabelCustomization class, and TextBoxCustomization class.
These are the functions available:


- **setTextFontName(String fontName)** -  sets the font type for a UI element. Example: 
```java
customization.setTextFontName("Font Name");
```

- **setTextColor(String hexColorCode)** -  sets the text color for a UI element. Example: 
```java
customization.setTextColor("#FFFFF");
```

- **setTextFontSize(int fontSize)** -  sets the font size for a UI element. Example: 
```java
customization.setTextFontSize(10);
```

- **getTextFontName()** - returns the font type for a UI element. Example: 
```java
String fontName = customization.getTextFontName();
```

- **getTextColor()** - returns the text color for a UI element. Example: 
```java
String textColor = customization.getTextColor();
```

- **getTextFontSize()** - returns the font size for a UI element. Example: 
```java
Int fontSize = customization.getTextFontSize();
```

All related customization classes follow the same format. That is, each extend the base Customization 
class and therefore you have access to the base functions above. In addition, there are specific functions 
available to customize the appearance of that entity/class (button, toolbar, label and text box). (See below)

2. Class ButtonCustomization


```java
public void setBackgroundColor(...) //Sets the background colour of the button.
public void setCornerRadius(...) //Sets the radius of the button corners.
public String getBackgroundColor() //Returns the background colour of the button.
public int getCornerRadius() //Returns the radius of the button corners.
```

3. Class ToolbarCustomization

```java
public void setBackgroundColor(...) //Sets the background colour for the toolbar.
public String getBackgroundColor() //Returns the background colour for the toolbar.
public void setHeaderText(...) //Sets the header text of the toolbar.
public String getHeaderText() //Returns the header text of the toolbar.
public void setButtonText(...) //Sets the button text of the toolbar.
public String getButtonText() //Returns the button text of the toolbar.
```

4. Class LabelCustomization

```java
public void setHeadingTextColor(...) //Sets the colour of the heading label text.
public void setHeadingTextFontName(...) //Sets the font type of the heading label text.
public void setHeadingTextFontSize(...) //Sets the font size of the heading label text.
public String getHeadingTextColor() //Returns the colour of the heading label text.
public String getHeadingTextFontName() //Returns the font type of the heading label text.
public int getHeadingTextFontSize() //Returns the font size of the heading label text.
```

5. Class TextBoxCustomization

```java
public void setBorderWidth(...) //Sets the width of the text box border.
public int getBorderWidth() //Returns the width of the text box border.
public void setBorderColor(...) //Sets the color of the text box border.
public String getBorderColor() //Returns the color of the text box border in hex colour code.
public void setCornerRadius(...) //Sets the corner radius of the text box corners.
public int getCornerRadius() //Gets the corner radius of the text box corners.
```



#### Example code snippet of initialize():
```java
//initialize all classes and define locale
ThreeDS threeDS = new ThreeDS();

//setup ConfigParameters
ConfigParameters configParameters = new ConfigParameters();
configParameters.addParam(ConfigParameters.DEFAULT_GROUP,"verbose","true");
configParameters.addParam(ConfigParameters.DEFAULT_GROUP,"mode","sandbox");
configParameters.addParam(ConfigParameters.DEFAULT_GROUP,"apiKey","123456789");

String locale = "en-US";
UiCustomization uiCustomization = new UiCustomization();


//call initialize of the ThreeDS instance
threeDS.initialize(this.activity, configParameters, locale, uiCustomization);
```

<a name="warnings"></a>
### 2a . SDK Warnings
During the initialize call, the SDK will run through a list of checks to 
determine the integrity of the device or the SDK itself adds them to a 
list of warnings that can be viewed by calling. These checks include:
```java
threeDS.getWarnings()
```


Below are a list of warnings and their corresponding Warning ID. 

| Security Warning ID        | Description           | Severity Level  |
| ------------- |:-------------:| -----:|
| SW01     | The device is jailbroken or rooted | HIGH |
| SW02      | The integrity of the SDK has been tampered      |   HIGH |
| SW03 | An emulator is being used to run the App      |    HIGH |
| SW04 | A debugger is attached to the App     |    MEDIUM |
| SW05 | The OS or OS version is not supported     |    HIGH |

Given an instance of the Warning class you call the following functions:


- **getId()** -  Returns the security warning ID. Example: 
    ```java
    String ID = warning.getId();
    ```
    
- **getMessage()** -  Returns the warning message. Example: 
    ```java
    String message = warning.getMessage();
    ```

- **getSeverity()** -  Returns the security level of the warning. Example: 
    ```java
    String severity = warning.getSeverity();
    ```



<a name="authentication"></a>
## Authentication 

The 3DS Requestor App collects the parameters that are required for authentication from the 3DS SDK and initiates the authentication flow. This includes 
data such as the transaction, the amount, credit card number, and the expiry month and year. This data is passed into the TransactionData class seen below.
```java
TransactionData transactionData = new TransactionData(clientTransactionId,amount,cardNumber,month,year);
```

The next step is to create a instance of transaction by calling createTransaction on the previously declared threeDS object. Then setting the transactionData on the
Transaction object:
```java
transaction = threeDS.createTransaction("","2.1.0");
transaction.setTransactionData(transactionData);
```

Finally, pass the Transaction object into the authenticate call:
```java
threeDS.authenticate(transaction);
```
This function will prepare everything needed for the Authentication request (AReq). A successful response
(Authentication Response - ARes) will contain and eci, xid and cavv.


<a name="challenge"></a>
## Challenge 

<a name="errors"></a>
## Errors
In addition to warnings, the SDK provides a wide range of errors. Below is a table of popular errors:


| Error/Exception Type       | Description           | Additional Info |
| ------------- |:-------------| :-----|
| SDK     | Neither PAN nor card token found in request | A credit card number was not submitted with the request|
| SDK     | No account found for API key | The API key provided is invalid|
| JSONException      | Indicates that some exception happened during JSON processing.      |
| InvalidInputException      |   Signals that an attempt to input an object from a stream has failed because an incorrect encoding was encountered    | |
| SDKNotInitializedException      | SDK not initialized   | |
| SDKRuntimeException      | This exception shall be thrown if an internal error is encountered by the 3DS SDK.   | |


